#!/bin/sh
exit(1)
HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
igroovePath=`/var/www/igroove`
service supervisor stop
echo "1/6 Aggiornamento composer"
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin
mv /usr/bin/composer.phar /usr/bin/composer
chmod a+x /usr/bin/composer
echo "2/6 Aggiornamento codice sorgente igroove"
rm -rf /tmp/igroove*.*
cd /tmp
wget --no-check-certificate https://bitbucket.org/vittorezen/igroove/get/master.tar.gz -O /tmp/igroove.tar.gz
gunzip /tmp/igroove.tar.gz
rm -rf /root/backup_igroove_config*
mkdir /root/backup_igroove_config
cp -r $igroovePath/app/config/* /root/backup_igroove_config
cp -r $igroovePath/googleAppsToken*.* /root/backup_igroove_config
mv $igroovePath/vendor /tmp/backup_igroove_vendor
rm -rf $igroovePath
mkdir $igroovePath
cd $igroovePath
tar -xf /tmp/igroove.tar --strip=1 --exclude="app/config/parameters.yml"  --exclude="app/config/igroove.yml"
rm -rf /tmp/igroove*.*
cp /root/backup_igroove_config/igroove.yml $igroovePath/app/config/
cp /root/backup_igroove_config/parameters.yml $igroovePath/app/config/
cp /root/backup_igroove_config/googleAppsToken*.* $igroovePath/
mv $igroovePath/web/app_dev.php $igroovePath/
mv /tmp/backup_igroove_vendor $igroovePath/vendor
mkdir $igroovePath/app/cache
mkdir $igroovePath/app/log
echo "3/6 Cambio permessi"
cd /var/www
chown -R $HTTPDUSER:$HTTPDUSER igroove
chmod -R 777 $igroovePath/app/log*
chmod -R 777 $igroovePath/app/cache
cd $igroovePath
echo "4/6 Aggiornamento componenti e librerie"
composer install
echo "5/6 Cancellazione cache"
rm -rf $igroovePath/app/cache/*
cd /var/www
chown -R $HTTPDUSER:$HTTPDUSER igroove
chmod -R 777 $igroovePath/app/log*
chmod -R 777 $igroovePath/app/cache
cd $igroovePath
echo "6/6 Update database schema"
php $igroovePath/app/console doctrine:schema:update --force
cd /var/www
service supervisor start
php $igroovePath/app/console version
echo "----- Finito ----"