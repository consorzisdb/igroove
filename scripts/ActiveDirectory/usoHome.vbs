homeFolder="\\srvc.issm.network\home\"


arrAttributes = Array("sAMAccountName","displayname","mail")

Set adoCommand = CreateObject("ADODB.Command")
Set adoConnection = CreateObject("ADODB.Connection")
adoConnection.Provider = "ADsDSOObject"
adoConnection.Open "Active Directory Provider"
adoCommand.ActiveConnection = adoConnection


 'On Error Resume Next


Set objRootDSE = GetObject("LDAP://RootDSE")
strBase = "<LDAP://" & objRootDSE.Get("defaultNamingContext") & ">"
Set objRootDSE = Nothing
set objFSO = CreateObject("Scripting.FileSystemObject")
set objTextFile = objFSO.CreateTextFile("c:\igroove\consumoDisco.csv", 8 , True)
strFilter = "(&(objectCategory=person)(objectClass=user))"
strAttributes = Join(arrAttributes,",")
strQuery = strBase & ";" & strFilter & ";" & strAttributes & ";subtree"
adoCommand.CommandText = strQuery
adoCommand.Properties("Page Size") = 100
adoCommand.Properties("Timeout") = 30
adoCommand.Properties("Cache Results") = False
Set adoRecordset = adoCommand.Execute
Do Until adoRecordset.EOF

 	 row=  adoRecordset.Fields("displayname").Value & ";" & adoRecordset.Fields("sAMAccountName").Value & " ; " & Foldersize (homeFolder & adoRecordset.Fields("sAMAccountName").Value) & "; MB"
objTextFile.WriteLine(row)
 ' Wscript.Echo row
	adoRecordset.MoveNext

Loop
objTextFile.Close
adoRecordset.Close
adoConnection.Close
Set adoRecordset = Nothing
Set adoConnection = Nothing
Set adoCommand = Nothing

Function Foldersize(strPath)
	On Error Resume Next
	Set objNetwork = CreateObject("WScript.Network")
	Set objFSO = CreateObject("scripting.filesystemobject")
        objNetwork.RemoveNetworkDrive "x:", true
	objNetwork.MapNetworkDrive "x:", strPath
	Set objFld = objFSO.GetFolder("x:")
	Foldersize = Round(objFld.Size/1048576,2)
	objNetwork.RemoveNetworkDrive "x:", true
	Set objFld = Nothing
	Set objFSO = Nothing
End Function