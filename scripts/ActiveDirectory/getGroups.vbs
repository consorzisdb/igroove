On Error Resume Next
Dim objConnection, objCommand, objRootDSE, strDNSDomain
Dim strFilter, strQuery, objRecordSet, gt
Set objConnection = CreateObject("ADODB.Connection")
Set objCommand = CreateObject("ADODB.Command")
objConnection.Provider = "ADsDSOOBject"
objConnection.Open "Active Directory Provider"
Set objCommand.ActiveConnection = objConnection
Set objRootDSE = GetObject("LDAP://RootDSE")
strDNSDomain = objRootDSE.Get("defaultNamingContext")
strBase = "<LDAP://" & strDNSDomain & ">"
strFilter = "(&(objectCategory=group))"
strAttributes = "sAMAccountName"
strQuery = strBase & ";" & strFilter & ";" & strAttributes & ";subtree"
objCommand.CommandText = strQuery
objCommand.Properties("Page Size") = 99999
objCommand.Properties("Timeout") = 300
objCommand.Properties("Cache Results") = False
Set objRecordSet = objCommand.Execute
objRecordSet.MoveFirst

Dim objFileSystem, objOutputFile
Dim strOutputFile

strOutputFile = "./" & Split(WScript.ScriptName, ".")(0) & ".txt"

Set objFileSystem = CreateObject("Scripting.fileSystemObject")
Set objOutputFile = objFileSystem.CreateTextFile(strOutputFile, TRUE) 


Do Until objRecordSet.EOF
    strSA = objRecordSet.Fields("sAMAccountName")
    objOutputFile.WriteLine(strSA) 
    objRecordSet.MoveNext
Loop



objConnection.Close
Set objConnection = Nothing
objOutputFile.Close

Set objFileSystem = Nothing

Set objCommand = Nothing
Set objRootDSE = Nothing
Set objRecordSet = Nothing