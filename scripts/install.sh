#!/bin/sh
echo "1/11 Aggiornamento pacchetti"
apt-get update -y
echo "2/11 Installazione dipendenze"
echo "deb http://www.rabbitmq.com/debian/ testing main" >> /etc/apt/sources.list.d/rabbitmq-server.list
wget https://www.rabbitmq.com/rabbitmq-signing-key-public.asc
apt-key add rabbitmq-signing-key-public.asc
echo "deb http://dl.ubnt.com/unifi/distros/deb/debian debian ubiquiti" >> /etc/apt/sources.list.d/unifi.list
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv C0A52C50

apt-get update -y
apt-get install php5-intl php5 php5-mysql php5-ldap php5-curl ldap-utils \
mysql-server php-pear rsync git-core curl acl freeradius-ldap php5-mcrypt \
freeradius-mysql libmail-pop3client-perl libio-socket-ssl-perl \
libnet-ssleay-perl python-pip rabbitmq-server  unifi-rapid -y
rabbitmq-plugins enable rabbitmq_management rabbitmq_tracing  rabbitmq_federation
service rabbitmq-server restart
echo "3/11 Installazione composer"
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin
mv /usr/bin/composer.phar /usr/bin/composer
chmod a+x /usr/bin/composer
echo "4/11 Installazione codice sorgente igroove"
rm -rf /tmp/igroove*.*
cd /tmp
wget --no-check-certificate https://bitbucket.org/vittorezen/igroove/get/master.tar.gz -O /tmp/igroove.tar.gz
gunzip /tmp/igroove.tar.gz
mkdir /root/backup_igroove_config
cp -r /var/www/igroove/app/config/* /root/backup_igroove_config
rm -rf /var/www/igroove
mkdir /var/www/igroove
cd /var/www/igroove
tar -xf /tmp/igroove.tar --strip=1 --exclude="app/config/security.yml" --exclude="app/config/igroove.yml" --exclude="app/config/dynamic.yml"
rm -rf /tmp/igroove*.*
echo "5/11 Copia file di configurazione"
cd /var/www/igroove
cp app/config/igroove.yml.dist app/config/igroove.yml
cp app/config/parameters.yml.dist app/config/parameters.yml
echo "6/12 Installazione vendor"
composer install

echo "6/11 Cambio permessi"
cd /var/www
chown -R www-data:www-data igroove
chmod -R 777 igroove
cd /var/www/igroove
echo "7/11 Installazione componenti e librerie"
composer install
echo "8/11 Cancellazione cache"
rm -rf /var/www/igroove/app/cache/*
cd /var/www
chown -R www-data:www-data igroove
chmod -R 777 igroove
cd /var/www/igroove
echo "9/11 Attivazione modulo apache2 rewrite"

a2enmod rewrite
service apache2 restart

echo "10/11 Attivazione timezone su php"

phptzapache=`grep "date.timezone = Europe/Rome" /etc/php5/apache2/php.ini -c `
if [ "$phptzapache" -lt 1 ]; then
echo  "date.timezone = Europe/Rome" >> /etc/php5/apache2/php.ini
fi 
phptzcli=`grep "date.timezone = Europe/Rome" /etc/php5/cli/php.ini -c `
if [ "$phptzcli" -lt 1 ]; then
echo  "date.timezone = Europe/Rome" >> /etc/php5/cli/php.ini
fi 


echo "11/11 Installazione servizi MQ consumer"
cp /var/www/igroove/linuxConfiguration//etc/rabbitmq/rabbitmq.config /etc/rabbitmq/rabbitmq.config
service rabbitmq-server restart
mkdir /var/log/supervisor
apt-get remove python-meld3 supervisor -y
apt-get install python-pip -y
pip install supervisor
mkdir /etc/supervisor
cp -r /var/www/igroove/linuxConfiguration/etc/supervisor/* /etc/supervisor
cp  /var/www/igroove/linuxConfiguration/etc/supervisord.conf /etc
cp  /var/www/igroove/linuxConfiguration/init.d/supervisor* /etc/init.d
chmod -R 777 /etc/supervisor*
systemctl daemon-reload
service supervisor restart
service supervisor status
