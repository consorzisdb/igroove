<?php

use App\Kernel;
use App\Manager\GoogleApps;
use Behat\Behat\Context\Context;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Webmozart\Assert\Assert;
use Behat\Gherkin\Node\TableNode;
use App\Entity\Provider;

class SystemContext implements Context
{
    /**
     * @var Application
     */
    private $application;

    /**
     * @var BufferedOutput
     */
    private $output;

    private static $container;

    public function __construct(Kernel $kernel)
    {
        $this->application = new Application($kernel);
        $this->output = new BufferedOutput();
        self::$container = $kernel->getContainer();
    }

    /**
     * @Given I am the system
     */
    public function iAmTheSystem()
    {
        Assert::same('cli', php_sapi_name());
    }

    /**
     * @When I run the command :command
     */
    public function iRunTheCommand($command)
    {
        $this->runCommand($command);
    }

    /**
     * @Then I should say :sentence
     */
    public function iShouldSay($sentence)
    {
        $output = $this->output->fetch();

        Assert::same($sentence, $output);
    }

    private function runCommand(string $command)
    {

        if ($command=='cron') {
            $gamMock = Mockery::mock(GoogleApps::class);
            $gamMock->shouldReceive('getUsersList')
                ->andReturn(null);
        }

        $input = new ArgvInput(['behat-test', $command, '--env=test']);
        $this->application->doRun($input, $this->output);
    }


    /**
     * @Given I hardly setup the provider :providerName with these parameters:
     */
    public function iHardlySetupTheProviderWithTheseParametersFor($providerName, TableNode $table)
    {

        $em = self::$container->get('doctrine')->getManager();
        $provider = $em->getRepository('App:Provider')->findOneByName($providerName);
        foreach ($table as $row) {
            $method = 'set' . ucfirst($row['key']);
            $provider->$method($row['value']);
        }
        $em->flush();
    }


    /**
     * @Then I should found in entity :entityName these records:
     */
    public function iShouldFoundInEntityTheseRecords($entityName, TableNode $table)
    {
        $em = self::$container->get('doctrine')->getManager();
        $repository = $em->getRepository('App:' . $entityName);
        foreach ($table as $criteria) {
            $entity = $repository->findOneBy($criteria);
            if (!$entity) {
                throw new Exception('Not found:' . json_encode($criteria));
            }

        }

    }


    /**
     * @Given all providers are disabled
     */
    public function allProvidersAreDisabled()
    {
        $em = self::$container->get('doctrine')->getManager();
        $providers = $em->getRepository('App:Provider')->findAll();
        foreach ($providers as $provider) {
            $provider->setActive(false);
        }
        $em->flush();
    }


    /**
     * @Given provider :providerName is correctly present, setup and active
     */
    public function providerCorrectlySetupAndPresent($providerName)
    {
        $em = self::$container->get('doctrine')->getManager();
        $provider = $em->getRepository('App:Provider')->findOneByName($providerName);
        if (!$provider) {
            $provider = new Provider();
            $provider->setName($providerName);
            $em->persist($provider);
        }

        switch ($providerName) {
            case "Studenti-csv":

                $provider->setFilter('csv');
                $provider->setStudentGoogleAppClientId('mestre.network-debug');
                $provider->setStudentGoogleAppDomain('mestre.network');
                $provider->setActive(1);
                $provider->setFilterData(['filename' => 'features/DataFixtures/Studenti-csv.csv']);
                break;
        }

        $em->flush();
    }


}