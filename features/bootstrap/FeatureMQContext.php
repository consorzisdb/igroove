<?php

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Behat\MinkExtension\Context\RawMinkContext;
use OldSound\RabbitMqBundle\RabbitMq\Consumer;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use PhpAmqpLib\Connection\AbstractConnection;
use PhpAmqpLib\Connection\AMQPLazyConnection;
use PhpAmqpLib\Exception\AMQPTimeoutException;
use PhpAmqpLib\Message\AMQPMessage;


class FeatureMQContext extends RawMinkContext
{
    /**
     * @var KernelInterface
     */
    private $kernel;

    private $message;

    /**
     * @var Response|null
     */
    private $response;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;

    }

    /**
     * @Then MQ system contains :message message in :queue queue
     */
    public function mqSystemContainsMessageInQueue($message, $queue)
    {

        $this->consumeMessage($queue);
        var_dump($this->message);
        throw new PendingException();
    }


    /**
     * @When supervisor MQ consumers are stopped
     */
    public function supervisorMqConsumersAreStopped()
    {
        throw new PendingException();
    }

    /**
     * @Given user :arg1 has two personal photos
     */
    public function userHasTwoPersonalPhotos($arg1)
    {
        throw new PendingException();
    }

    /**
     * @When I send to MQ system :arg1 message in :arg2 queue
     */
    public function iSendToMqSystemMessageInQueue($arg1, $arg2)
    {
        throw new PendingException();
    }


    /**
     * @return \PhpAmqpLib\Connection\AbstractConnection
     */
    private function getConnection($queue)
    {
        return $this->kernel->getContainer()->get('old_sound_rabbit_mq.connection.' . $queue);
    }


    private function requiresRabbitMQ($queue)
    {
        $connection = $this->getConnection($queue);
        if ($connection instanceof AMQPLazyConnection) {
            try {
                $connection->reconnect();
            } catch (\ErrorException $e) {
            }
        }
        if (!$connection->isConnected()) {
            throw new PendingException('RabbitMQ is not available.');
        }
    }

    /**
     * @return ConsumerInterface
     */
    private function getConsumer(string $queue)
    {
        $consumer = new Consumer($this->getConnection($queue));
        $consumer->setExchangeOptions(['name' => $queue, 'type' => 'direct']);
        $consumer->setQueueOptions(['name' => $queue]);
        $consumer->setCallback([$this, 'handleMessage']);
        $consumer->setIdleTimeout(2);
        return $consumer;
    }

    private function consumeMessage(string $queue)
    {
        try {
            echo 111;
            $this->getConsumer($queue)->consume(1);
        } catch (AMQPTimeoutException $e) {
            $this->fail('Failed to consume a message: ' . $e->getMessage());
        }
    }

    public function handleMessage(AMQPMessage $message)
    {
        var_dump($message);die;
        $this->consumedMessage = $message;
    }
}
