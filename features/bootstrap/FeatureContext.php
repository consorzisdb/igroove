<?php

use Behat\Gherkin\Node\TableNode;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Knp\FriendlyContexts\Context\MinkContext;
use Behat\Behat\Context\TranslatableContext;
use Knp\FriendlyContexts\Utils\Asserter;
use Knp\FriendlyContexts\Utils\TextFormater;

/**
 * This context class contains the definitions of the steps used by the demo
 * feature file. Learn how to get started with Behat and BDD on Behat's website.
 *
 * @see http://behat.org/en/latest/quick_start.html
 */
class FeatureContext extends MinkContext implements TranslatableContext
{
    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @var Response|null
     */
    private $response;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @Given I am logged as :user with :password
     * @Given I am logged as user :user with password :password
     */
    public function iAmLoggedAsWith($user, $password)
    {
        $this->printLastResponse();
        $this->visitPath('/login');
        $this->fillField('username', $user);
        $this->fillField('password', $password);
        $this->pressButton('Accedi');
    }

    /**
     * @When wait :arg1 seconds
     */
    public function waitSeconds($arg1)
    {
        sleep($arg1);
    }

    private function getUserSession($username):\Behat\Mink\Session
    {
        if (!$this->getMink()->hasSession(strtolower($username))) {
            $driver=new \Behat\Symfony2Extension\Driver\KernelDriver($this->kernel);
            $this->getMink()->registerSession(strtolower($username), new \Behat\Mink\Session($driver));
        }
        return $this->getSession(strtolower($username));
    }

    private function LoginWithDifferentSession(\Behat\Mink\Session $session,$username, $password)
    {

        $session->visit('/login');

        $session->getPage()->fillField('_username', $username);
        $session->getPage()->fillField('_password', $password);
        $session->getPage()->pressButton('Accedi');
    }


    /**
     * @Given user :username is logged with password :password
     */
    public function userIsLoggedWithPassword($username, $password)
    {
        $userSession=$this->getUserSession($username);
        $this->LoginWithDifferentSession($userSession,$username, $password);
    }

    /**
     * @Given user :user is on the homepage
     */
    public function userIsOnTheHomepage($user)
    {
        $userSession=$this->getUserSession($user);
        $userSession->visit('/');
    }

    /**
     * @Given user :user follow :link
     */
    public function userFollow($user, $link)
    {
        $user = strtolower($user);
        $link = $this->fixStepArgument($link);
        $this->getUserSession($user)->getPage()->clickLink($link);
    }


    /**
     * @When user :username should see :text
     */
    public function userShouldSee($username, $text)
    {
        $this->assertSession($username)->pageTextContains($this->fixStepArgument($text));
    }

    /**
     * @When user :username fill in :field with :value
     */
    public function userFillInWith($username, $field, $value)
    {
        $field = $this->fixStepArgument($field);
        $value = $this->fixStepArgument($value);
        $userSession=$this->getUserSession($username);
        $userSession->getPage()->fillField($field, $value);
    }

    /**
     * @When user :username attach the file :file to :field
     */
    public function userAttachTheFileTo($username, $file, $field)
    {
        $user = strtolower($username);
        $file = $this->fixStepArgument($file);
        $field = $this->fixStepArgument($field);
        $this->getUserSession($user)->getPage()->attachFileToField($field,$file);
    }

    /**
     * @When user :username press :button
     */
    public function userPress($username, $button)
    {
        $user = strtolower($username);
        $button = $this->fixStepArgument($button);
        $this->getUserSession($user)->getPage()->pressButton($button);
    }

    /**
     * @Then I am redirected to login page
     */
    public function iAmRedirectedToLoginPage()
    {
     $this->assertUrlRegExp('/\/login$/m');
    }
    /**
     * @Then /^I should see a table with "([^"]*)" in the "([^"]*)" column$/
     */
    public function iShouldSeeATableWithInTheNamedColumn($list, $column)
    {
        $cells = array_merge(array($column), $this->getFormater()->listToArray($list));
        $expected = new TableNode(array_map(function($cell) { return [$cell]; }, $cells));

        $this->iShouldSeeTheFollowingTable($expected);
    }

    /**
     * @Given /^I should see the following table:?$/
     */
    public function iShouldSeeTheFollowingTable($expected)
    {
        if ($expected instanceof TableNode) {
            $expected = $expected->getTable();
        }

        $this->iShouldSeeATable();

        $tables = $this->findTables();
        $exceptions = array();

        foreach ($tables as $table) {
            try {
                if (false === $extraction = $this->extractColumns(current($expected), $table)) {
                    $this->getAsserter()->assertArrayEquals($expected, $table, true);

                    return;
                }
                $this->getAsserter()->assertArrayEquals($expected, $extraction, true);

                return;
            } catch (\Exception $e) {
                $exceptions[] = $e;
            }
        }

        $message = implode("\n", array_map(function ($e) { return $e->getMessage(); }, $exceptions));

        throw new \Exception($message);
    }

    /**
     * @Then /^I should see the following table portion:?$/
     */
    public function iShouldSeeTheFollowingTablePortion($expected)
    {
        if ($expected instanceof TableNode) {
            $expected = $this->reorderArrayKeys($expected->getTable());
        }

        $this->iShouldSeeATable();

        $tables = $this->findTables();
        $exceptions = array();

        foreach ($tables as $table) {
            try {
                if (false === $extraction = $this->extractColumns(current($expected), $table)) {
                    $this->getAsserter()->assertArrayContains($expected, $table);

                    return;
                }
                $this->getAsserter()->assertArrayContains($expected, $extraction);

                return;
            } catch (\Exception $e) {
                $exceptions[] = $e;
            }
        }

        $message = implode("\n", array_map(function ($e) { return $e->getMessage(); }, $exceptions));

        throw new \Exception($message);
    }

    /**
     * @Then /^I should see a table with ([^"]*) rows$/
     * @Then /^I should see a table with ([^"]*) row$/
     */
    public function iShouldSeeATableWithRows($nbr)
    {
        $nbr = (int) $nbr;

        $this->iShouldSeeATable();
        $exceptions = array();
        $tables = $this->getSession()->getPage()->findAll('css', 'table');

        foreach ($tables as $table) {
            try {
                if (null !== $body = $table->find('css', 'tbody')) {
                    $table = $body;
                }
                $rows = $table->findAll('css', 'tr');
                $this->getAsserter()->assertEquals($nbr, count($rows), sprintf('Table with %s rows expected, table with %s rows found.', $nbr, count($rows)));
                return;
            } catch (\Exception $e) {
                $exceptions[] = $e;
            }
        }

        $message = implode("\n", array_map(function ($e) { return $e->getMessage(); }, $exceptions));

        throw new \Exception($message);
    }

    /**
     * @Then /^I should see a table$/
     */
    public function iShouldSeeATable()
    {
        try {
            $this->getSession()->wait(2000, '0 < document.getElementsByTagName("TABLE").length');
        } catch (\Exception $ex) {
            unset($ex);
        }
        $tables = $this->getSession()->getPage()->findAll('css', 'table');
        $this->getAsserter()->assert(0 < count($tables), 'No table found');
    }

    protected function extractColumns(array $headers, array $table)
    {
        if (0 == count($table) || 0 == count($headers)) {
            return false;
        }

        $columns = array();
        $tableHeaders = current($table);
        foreach ($headers as $header) {
            $inArray = false;
            foreach ($tableHeaders as $index => $thead) {
                if ($thead === $header) {
                    $columns[] = $index;
                    $inArray = true;
                }
            }
            if (false === $inArray) {
                return false;
            }
        }

        $result = array();
        foreach ($table as $row) {
            $node = array();
            foreach ($row as $index => $value) {
                if (in_array($index, $columns)) {
                    $node[] = $value;
                }
            }
            $result[] = $node;
        }

        return $result;
    }

    protected function findTables()
    {
        $tables = $this->getSession()->getPage()->findAll('css', 'table');
        $result = array();

        foreach ($tables as $table) {
            $node = array();
            $head = $table->find('css', 'thead');
            $body = $table->find('css', 'tbody');
            $foot = $table->find('css', 'tfoot');
            if (null !== $head || null !== $body || null !== $foot) {
                $this->extractDataFromPart($head, $node);
                $this->extractDataFromPart($body, $node);
                $this->extractDataFromPart($foot, $node);
            } else {
                $this->extractDataFromPart($table, $node);
            }
            $result[] = $node;
        }

        return $result;
    }

    protected function extractDataFromPart($part, &$array)
    {
        if (null === $part) {

            return;
        }

        foreach ($part->findAll('css', 'tr') as $row) {
            $array[] = $this->extractDataFromRow($row);
        }
    }

    protected function extractDataFromRow($row)
    {
        $result = array();
        $elements = array_merge($row->findAll('css', 'th'), $row->findAll('css', 'td'));

        foreach ($elements as $element) {
            $result[] = preg_replace('!\s+!', ' ', $element->getText());
        }

        return $result;
    }

    protected function reorderArrayKeys(array $subject)
    {
        $orderedArray = array();

        foreach ($subject as $key => $value) {
            if (is_int($key)) {
                $orderedArray[] = $value;
            } else {
                $orderedArray[$key] = $value;
            }
        }

        return $orderedArray;
    }

    protected function getAsserter()
    {
        return new Asserter($this->getFormater());
    }

    protected function getFormater()
    {
        return new TextFormater;
    }
}
