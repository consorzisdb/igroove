# homepage.feature
Feature: User homepage
  As a registered user
  I see certain links based on my role

  Scenario: link ok
    Given I am on the homepage
    And I am logged as user "administrator" with password "2wsx-1qaz"
    When I follow "Gestione ip e dispositivi"
    Then I should be on "/device"


  Scenario: elenco ok
    Given I am on the homepage
    And I am logged as user "administrator" with password "2wsx-1qaz"
    And I follow "Gestione ip e dispositivi"
    Then I should see the following table portion:
      | Mac               | Dispositivo                  | Lista | IPs | Bypass su hotspot | Attivo|
      | 8C:88:2B:60:08:C6 | Antenna WIFI Riccardo Beggio |       |     |                   |       |
      | 9C:20:7B:CD:57:77 | Apple TV                     |       |     |                   |       |
      | 9C:20:7B:C6:30:AE | apple tv                     |       |     |                   |       |


  Scenario: aggiungi nuovo
    Given I am on the homepage
    And I am logged as user "administrator" with password "2wsx-1qaz"
    And I follow "Gestione ip e dispositivi"
    And I follow "Aggiungi un nuovo dispositivo"
    And I should be on "/device/new"
    Then I should see "Dispositivo"
    And I should see "Indirizzo MAC"
    And I should see "Lista IP su mikrotik (opzionale)"
    And I should see "Indirizzi IP riservati (opzionale)"
    And I should see "Attiva bypass su hotspot"
    And I should see "Attivo"
    And I should see "Aggiungi questo dispositivo"
    And I should see "Ritorna alla lista"
    And the checkbox "device[bypassHotspot]" should be unchecked
    And the checkbox "device[active]" should be checked


  Scenario: rimuovi un dispositivo
    Given I am on the homepage
    And I am logged as user "administrator" with password "2wsx-1qaz"
    And I follow "Gestione ip e dispositivi"
    And I follow "EC:56:23:01:60:C6"
    And I should be on "/device/4e27db08-c8cd-11e9-898d-000c29207325/edit"
    Then I should see "Rimuovi l'accesso per questo dispositivo"


