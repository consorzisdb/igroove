import '../scss/index.scss';

import './index.js';

import '../images/sf.png';
import '../images/bg.jpg';
import '../images/404.png';
import '../images/500.png';
import '../images/logo.png';
import '../images/profile.png';


import '../images/datatables/sort_asc.png';
import '../images/datatables/sort_asc_disabled.png';
import '../images/datatables/sort_both.png';
import '../images/datatables/sort_desc.png';
import '../images/datatables/sort_desc_disabled.png';

import '../fonts/icons/fontawesome/fontawesome-webfont.eot';
import '../fonts/icons/fontawesome/fontawesome-webfont.woff';
import '../fonts/icons/fontawesome/fontawesome-webfont.woff2';
import '../fonts/icons/fontawesome/fontawesome-webfont.svg';
import '../fonts/icons/fontawesome/fontawesome-webfont.ttf';

import '../fonts/icons/themify/themify.eot';
import '../fonts/icons/themify/themify.svg';
import '../fonts/icons/themify/themify.ttf';
import '../fonts/icons/themify/themify.woff';


import $ from 'jquery';
window.jQuery = $;
window.$ = $;

global.$ = global.jQuery = $;



const imagesContext = require.context('../images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);