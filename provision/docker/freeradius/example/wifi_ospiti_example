server wifi_ospiti {

	listen {
		ipaddr = *
		port = 1832
		type = auth
	}

	authorize {
		preprocess

		#
		#  If you want to have a log of authentication requests,
		#  un-comment the following line, and the 'detail auth_log'
		#  section, above.
		#auth_log

		#
		#  The chap module will set 'Auth-Type := CHAP' if we are
		#  handling a CHAP request and Auth-Type has not already been set
		chap

		#
		#  If the users are logging in with an MS-CHAP-Challenge
		#  attribute for authentication, the mschap module will find
		#  the MS-CHAP-Challenge attribute, and add 'Auth-Type := MS-CHAP'
		#  to the request, which will cause the server to then use
		#  the mschap module for authentication.
		mschap

		#
		#  If you have a Cisco SIP server authenticating against
		#  FreeRADIUS, uncomment the following line, and the 'digest'
		#  line in the 'authenticate' section.
		digest

		#
		#  If you are using multiple kinds of realms, you probably
		#  want to set "ignore_null = yes" for all of them.
		#  Otherwise, when the first style of realm doesn't match,
		#  the other styles won't be checked.
		#
		suffix
		#ntdomain

		#
		#  This module takes care of EAP-MD5, EAP-TLS, and EAP-LEAP
		#  authentication.
		#
		#  It also sets the EAP-Type attribute in the request
		#  attribute list to the EAP type from the packet.
		#
		#  As of 2.0, the EAP module returns "ok" in the authorize stage
		#  for TTLS and PEAP.  In 1.x, it never returned "ok" here, so
		#  this change is compatible with older configurations.
		#
		#  The example below uses module failover to avoid querying all
		#  of the following modules if the EAP module returns "ok".
		#  Therefore, your LDAP and/or SQL servers will not be queried
		#  for the many packets that go back and forth to set up TTLS
		#  or PEAP.  The load on those servers will therefore be reduced.
		#
		#eap {
		eap_wifi_ospiti {
			ok = return
		}

		#
		#  Pull crypt'd passwords from /etc/passwd or /etc/shadow,
		#  using the system API's to get the password.  If you want
		#  to read /etc/passwd or /etc/shadow directly, see the
		#  passwd module in radiusd.conf.
		#
		#unix

		#
		#  Read the 'users' file
		files

		#guest
		if(Realm == "guest") {
			#verifica che il nome utente inizi con guest e un numero
			if(Stripped-User-Name =~ /guest([0-9]+)/i) {
				#verifica tramite query l'esistenza del guest e la sua validità temporale
				if("%{sql:SELECT id FROM `Guest` WHERE id = '%{1}' AND starting_from <= CURDATE() AND ending_to >= CURDATE()}" > 0 ) {
					update control {
						Cleartext-Password := "%{sql:SELECT password FROM `Guest` WHERE id = %{1}}"
						MS-CHAP-Use-NTLM-Auth := No
					}
					ok
				}
				else {
					update reply {
						Reply-Message = "Credenziali non valide o scadute"
					}
					fail
				}
			}
		}
		else {
			ldap

			if(ok || updated) {
				#permette l'accesso se si fa parte del gruppo AD WifiGuest e il proprio mac-address è registrato in igroove
				if(Ldap-Group == "WifiGuest" && request:Calling-Station-Id =~ /([0-9a-f]{2})[-:]?([0-9a-f]{2})[-:.]?([0-9a-f]{2})[-:]?([0-9a-f]{2})[-:.]?([0-9a-f]{2})[-:]?([0-9a-f]{2})/i) {
					if("%{sql:SELECT id FROM `Device` WHERE LOWER(mac) = '%{tolower:%{1}:%{2}:%{3}:%{4}:%{5}:%{6}}' AND active = 1}" != "" ) {
						ok
					}
					else {
						fail
					}
				}
				#controlla se lo studente è abilitato (nel gruppo AD InternetAccess) e può usare dispositivi personali (nel gruppo AD PersonalDeviceAccess)
				elsif(Ldap-Group == "InternetAccess" && Ldap-Group == "PersonalDeviceAccess") {
					ok
				}
				else {
					fail
				}
			}
		}

		#se non ha trovato niente, o non si era parte del gruppo giusto, non accettare
		if(!ok && !updated) {
			reject
		}

		#impostazione per vlan dinamica antenna
		update reply {
			Tunnel-Type = 13
			Tunnel-Medium-Type = 6
			Tunnel-Private-Group-Id := "302" #vlan default
		}


		#
		#  Look in an SQL database.  The schema of the database
		#  is meant to mirror the "users" file.
		#
		#  See "Authorization Queries" in sql.conf
		#sql

		#
		#  If you are using /etc/smbpasswd, and are also doing
		#  mschap authentication, the un-comment this line, and
		#  configure the 'smbpasswd' module.
		#smbpasswd

		#
		#  The ldap module will set Auth-Type to LDAP if it has not
		#  already been set
		#ldap

		#
		#  Enforce daily limits on time spent logged in.
		#daily

		#
		# Use the checkval module
		#checkval

		expiration
		logintime

		#
		#  If no other module has claimed responsibility for
		#  authentication, then try to use PAP.  This allows the
		#  other modules listed above to add a "known good" password
		#  to the request, and to do nothing else.  The PAP module
		#  will then see that password, and use it to do PAP
		#  authentication.
		#
		#  This module should be listed last, so that the other modules
		#  get a chance to set Auth-Type for themselves.
		#
		#pap

		#
		#  If "status_server = yes", then Status-Server messages are passed
		#  through the following section, and ONLY the following section.
		#  This permits you to do DB queries, for example.  If the modules
		#  listed here return "fail", then NO response is sent.
		#
	#	Autz-Type Status-Server {
	#
	#	}
	}


	#  Authentication.
	#
	#
	#  This section lists which modules are available for authentication.
	#  Note that it does NOT mean 'try each module in order'.  It means
	#  that a module from the 'authorize' section adds a configuration
	#  attribute 'Auth-Type := FOO'.  That authentication type is then
	#  used to pick the apropriate module from the list below.
	#

	#  In general, you SHOULD NOT set the Auth-Type attribute.  The server
	#  will figure it out on its own, and will do the right thing.  The
	#  most common side effect of erroneously setting the Auth-Type
	#  attribute is that one authentication method will work, but the
	#  others will not.
	#
	#  The common reasons to set the Auth-Type attribute by hand
	#  is to either forcibly reject the user (Auth-Type := Reject),
	#  or to or forcibly accept the user (Auth-Type := Accept).
	#
	#  Note that Auth-Type := Accept will NOT work with EAP.
	#
	#  Please do not put "unlang" configurations into the "authenticate"
	#  section.  Put them in the "post-auth" section instead.  That's what
	#  the post-auth section is for.
	#
	authenticate {
		#
		#  PAP authentication, when a back-end database listed
		#  in the 'authorize' section supplies a password.  The
		#  password can be clear-text, or encrypted.
		Auth-Type PAP {
			pap
		}

		#
		#  Most people want CHAP authentication
		#  A back-end database listed in the 'authorize' section
		#  MUST supply a CLEAR TEXT password.  Encrypted passwords
		#  won't work.
		Auth-Type CHAP {
			chap
		}

		#
		#  MSCHAP authentication.
		Auth-Type MS-CHAP {
			mschap
		}

		mschap

		#
		#  If you have a Cisco SIP server authenticating against
		#  FreeRADIUS, uncomment the following line, and the 'digest'
		#  line in the 'authorize' section.
		digest

		#
		#  Pluggable Authentication Modules.
		#pam

		#
		#  See 'man getpwent' for information on how the 'unix'
		#  module checks the users password.  Note that packets
		#  containing CHAP-Password attributes CANNOT be authenticated
		#  against /etc/passwd!  See the FAQ for details.
		#
		#  For normal "crypt" authentication, the "pap" module should
		#  be used instead of the "unix" module.  The "unix" module should
		#  be used for authentication ONLY for compatibility with legacy
		#  FreeRADIUS configurations.
		#
		#unix

		# Uncomment it if you want to use ldap for authentication
		#
		# Note that this means "check plain-text password against
		# the ldap database", which means that EAP won't work,
		# as it does not supply a plain-text password.
	#	Auth-Type LDAP {
	#		ldap
	#	}

		#ntlm_auth


		#
		#  Allow EAP authentication.
		#eap
		eap_wifi_ospiti

		#
		#  The older configurations sent a number of attributes in
		#  Access-Challenge packets, which wasn't strictly correct.
		#  If you want to filter out these attributes, uncomment
		#  the following lines.
		#
	#	Auth-Type eap {
	#		eap {
	#			handled = 1
	#		}
	#		if (handled && (Response-Packet-Type == Access-Challenge)) {
	#			attr_filter.access_challenge.post-auth
	#			handled  # override the "updated" code from attr_filter
	#		}
	#	}
	}



	#  Session database, used for checking Simultaneous-Use. Either the radutmp
	#  or rlm_sql module can handle this.
	#  The rlm_sql module is *much* faster
	session {
		radutmp

		#
		#  See "Simultaneous Use Checking Queries" in sql.conf
	#	sql
	}


	#  Post-Authentication
	#  Once we KNOW that the user has been authenticated, there are
	#  additional steps we can take.
	post-auth {
		#  Get an address from the IP Pool.
	#	main_pool

		#
		#  If you want to have a log of authentication replies,
		#  un-comment the following line, and the 'detail reply_log'
		#  section, above.
	#	reply_log

		#
		#  After authenticating the user, do another SQL query.
		#
		#  See "Authentication Logging Queries" in sql.conf
	#	sql

		#
		#  Instead of sending the query to the SQL server,
		#  write it into a log file.
		#
	#	sql_log

		#
		#  Un-comment the following if you have set
		#  'edir_account_policy_check = yes' in the ldap module sub-section of
		#  the 'modules' section.
		#
	#	ldap

		# For Exec-Program and Exec-Program-Wait
		exec

		#
		#  Calculate the various WiMAX keys.  In order for this to work,
		#  you will need to define the WiMAX NAI, usually via
		#
		#	update request {
		#	       WiMAX-MN-NAI = "%{User-Name}"
		#	}
		#
		#  If you want various keys to be calculated, you will need to
		#  update the reply with "template" values.  The module will see
		#  this, and replace the template values with the correct ones
		#  taken from the cryptographic calculations.  e.g.
		#
		# 	update reply {
		#		WiMAX-FA-RK-Key = 0x00
		#		WiMAX-MSK = "%{EAP-MSK}"
		#	}
		#
		#  You may want to delete the MS-MPPE-*-Keys from the reply,
		#  as some WiMAX clients behave badly when those attributes
		#  are included.  See "raddb/modules/wimax", configuration
		#  entry "delete_mppe_keys" for more information.
		#
	#	wimax

		#  If there is a client certificate (EAP-TLS, sometimes PEAP
		#  and TTLS), then some attributes are filled out after the
		#  certificate verification has been performed.  These fields
		#  MAY be available during the authentication, or they may be
		#  available only in the "post-auth" section.
		#
		#  The first set of attributes contains information about the
		#  issuing certificate which is being used.  The second
		#  contains information about the client certificate (if
		#  available).
	#
	#	update reply {
	#	       Reply-Message += "%{TLS-Cert-Serial}"
	#	       Reply-Message += "%{TLS-Cert-Expiration}"
	#	       Reply-Message += "%{TLS-Cert-Subject}"
	#	       Reply-Message += "%{TLS-Cert-Issuer}"
	#	       Reply-Message += "%{TLS-Cert-Common-Name}"
	#	       Reply-Message += "%{TLS-Cert-Subject-Alt-Name-Email}"
	#
	#	       Reply-Message += "%{TLS-Client-Cert-Serial}"
	#	       Reply-Message += "%{TLS-Client-Cert-Expiration}"
	#	       Reply-Message += "%{TLS-Client-Cert-Subject}"
	#	       Reply-Message += "%{TLS-Client-Cert-Issuer}"
	#	       Reply-Message += "%{TLS-Client-Cert-Common-Name}"
	#	       Reply-Message += "%{TLS-Client-Cert-Subject-Alt-Name-Email}"
	#	}

		#  MacSEC requires the use of EAP-Key-Name.  However, we don't
		#  want to send it for all EAP sessions.  Therefore, the EAP
		#  modules put required data into the EAP-Session-Id attribute.
		#  This attribute is never put into a request or reply packet.
		#
		#  Uncomment the next few lines to copy the required data into
		#  the EAP-Key-Name attribute
	#	if (reply:EAP-Session-Id) {
	#		update reply {
	#			EAP-Key-Name := "%{reply:EAP-Session-Id}"
	#		}
	#	}

		#  If the WiMAX module did it's work, you may want to do more
		#  things here, like delete the MS-MPPE-*-Key attributes.
		#
		#	if (updated) {
		#		update reply {
		#			MS-MPPE-Recv-Key !* 0x00
		#			MS-MPPE-Send-Key !* 0x00
		#		}
		#	}

		#
		#  Access-Reject packets are sent through the REJECT sub-section of the
		#  post-auth section.
		#
		#  Add the ldap module name (or instance) if you have set
		#  'edir_account_policy_check = yes' in the ldap module configuration
		#
		Post-Auth-Type REJECT {
			# log failed authentications in SQL, too.
	#		sql
			attr_filter.access_reject
		}
	}

}
