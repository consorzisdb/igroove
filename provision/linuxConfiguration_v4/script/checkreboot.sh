#!/usr/bin/env bash

if [ -f /tmp/reboot.this ]; then
  rm -f /tmp/reboot.this
  /sbin/shutdown -r now
fi