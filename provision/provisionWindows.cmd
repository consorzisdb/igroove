echo 1/3 Installazione componenti
powershell -command "Install-WindowsFeature     -Name AD-Domain-Services     -IncludeManagementTools"
echo 2/3 Importazione modulo ADDSDeployment
powershell -command "Import-Module ADDSDeployment"
echo 3/3 Installazione AD
powershell -command " $my_secure_password = convertto-securestring Secret-PASS -asplaintext -force; Install-ADDSForest -CreateDnsDelegation:$false -DatabasePath C:\Windows\NTDS -DomainMode Win2012R2 -DomainName igroove.network -DomainNetbiosName igroove -ForestMode Win2012R2 -InstallDns:$true -LogPath C:\Windows\NTDS -NoRebootOnCompletion:$false -SysvolPath C:\Windows\SYSVOL -Force:$true -SafeModeAdministratorPassword $my_secure_password"
echo Fine!