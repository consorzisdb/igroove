<?php

namespace App\Consumer;

use App\Entity\LdapUser;
use App\Manager\ConfigurationManager;
use App\Manager\LdapProxy;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use App\Entity\PersonAbstract;
use App\Exception\ConnectionException;
use App\Manager\GoogleApps;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;

class GoogleAppsConsumer extends AbstractConsumer
{
    /**
     * @var GoogleApps
     */
    private $googleApps;
    private $kernelRootDir;
    private $em;
    private $startTime;
    private $logger;
    protected $configurationManager;
    protected $ldapProxy;

    public function __construct($kernelRootDir, EntityManager $em, LoggerInterface $logger, ConfigurationManager $configurationManager, LdapProxy $ldapProxy)
    {
        $this->kernelRootDir = $kernelRootDir;
        $this->em = $em;
        $this->startTime = time();
        $this->logger = $logger;
        $this->configurationManager = $configurationManager;
        $this->ldapProxy = $ldapProxy;
    }

    public function execute(AMQPMessage $msg)
    {
        $message = unserialize($msg->body);
        if (!array_key_exists('command', $message)) {
            return true;
        }

        $keys = array_keys($message['parameters']);
        $allKeysPresent = (sizeof(array_intersect($keys, array('domain', 'clientId', 'clientSecret', 'ouPath'))) == 4);
        if (!$allKeysPresent) {
            $this->logger->error("GOOGLEAPPSCONS: Invalid configuration data");
            return true;
        }

        $this->em->clear();
        if(time()-$this->startTime > 600) {
            $this->em->getConnection()->close();
            $this->em->getConnection()->connect();
        }

        $this->googleApps = new GoogleApps(
            $message['parameters']['domain'],
            $message['parameters']['clientId'],
            $message['parameters']['clientSecret'],
            $message['parameters']['ouPath'],
            $this->kernelRootDir,
            $this->logger
        );

        try {
            switch ($message['command']) {
                case 'createUser':
                    $this->createUser($message['parameters']['email'], isset($message['parameters']['firstname']) ? $message['parameters']['firstname'] : NULL, isset($message['parameters']['lastname']) ? $message['parameters']['lastname'] : NULL, isset($message['parameters']['password']) ? $message['parameters']['password'] : NULL, isset($message['parameters']['userId']) ? $message['parameters']['userId'] : NULL, isset($message['parameters']['teacher']) ? (bool)$message['parameters']['teacher'] : FALSE);
                    break;

                case 'renameUser':
                    $this->renameUser($message['parameters']['email'], $message['parameters']['previousEmail'], isset($message['parameters']['firstname']) ? $message['parameters']['firstname'] : NULL, isset($message['parameters']['lastname']) ? $message['parameters']['lastname'] : NULL, isset($message['parameters']['userId']) ? $message['parameters']['userId'] : NULL, isset($message['parameters']['teacher']) ? (bool)$message['parameters']['teacher'] : FALSE);
                    break;

                case 'createGroup':
                    $this->googleApps->createGroup($message['parameters']['groupName']);
                    break;

                case 'renameGroup':
                    $this->googleApps->renameGroup($message['parameters']['groupName'], $message['parameters']['previousGroupName']);
                    break;

                case 'createOU':
                    $this->googleApps->createOU($message['parameters']['ouName']);
                    break;

                case 'addUserToGroup':
                    $this->googleApps->addUserToGroup($message['parameters']['email'], $message['parameters']['groupName']);
                    break;

                case 'addUserToOU':
                    $this->googleApps->addUserToOU($message['parameters']['email'], $message['parameters']['ouName']);
                    break;

                case 'updateGroupMembers':
                    $this->googleApps->updateGroupMembers($message['parameters']['studentsInGroup'], $message['parameters']['groupName']);
                    break;

                case 'updateOUMembers':
                    $this->googleApps->updateOUMembers($message['parameters']['studentsInOU'], $message['parameters']['ouName']);
                    break;

                case 'resetUserPassword':
                    $this->googleApps->resetUserPassword($message['parameters']['email'], $message['parameters']['password']);
                    break;

                case 'removeUser':
                    $this->googleApps->removeUser($message['parameters']['email']);
                    break;

                case 'removeGroup':
                    $this->googleApps->removeGroup($message['parameters']['groupName']);
                    break;
            }
        } catch (\Throwable $e) {
            return $this->retryOnError($msg, $e);
        }

        return ConsumerInterface::MSG_ACK;
    }

    private function createUser($email, $firstname, $lastname, $password, $userId, $teacher)
    {
        $this->googleApps->createUser($email, $firstname, $lastname, $password);

        if($userId != "") {
            $this->updateLdapUser($email, $userId, $teacher);
        }
    }

    private function renameUser($email, $previousEmail, $firstname, $lastname, $userId, $teacher)
    {
        $this->googleApps->renameUser($email, $previousEmail, $firstname, $lastname);

		if($userId != "") {
			$this->updateLdapUser($email, $userId, $teacher);
		}
    }

    protected function updateLdapUser($email, $userId, $teacher) {
		$person = $this->em->getRepository($teacher ? 'App:Teacher' : 'App:Student')->find($userId);
		if(!$person instanceof PersonAbstract) {
			return;
		}

		$person->setEmail((strpos($email, '@') === false) ? $email . '@' . $this->googleApps->getDomain() : $email);

		$ldapUser = $this->em->getRepository("App:LdapUser")->findOneByDistinguishedId($userId);
		if($ldapUser instanceof LdapUser) {
			$ldapUser->setAttribute('email', $email);
			$providerSettings = $person->getProviderSettings();
			if($providerSettings['ldapUseEmailForExtendedUsername']) {
				$extendedUsername = strtolower(substr($person->getEmail(), 0, strpos($person->getEmail(), '@')));
				if($providerSettings['ldapExtendedUsernameSuffix'] != "") {
					$extendedUsername .= (substr($providerSettings['ldapExtendedUsernameSuffix'],0,1)!="@"?"@":""). $providerSettings['ldapExtendedUsernameSuffix'];
				} else {
					$extendedUsername .= (substr($this->configurationManager->getActiveDirectoryAccountSuffix(),0,1)!="@"?"@":"").$this->configurationManager->getActiveDirectoryAccountSuffix();
				}
				$ldapUser->setAttribute('extendedUsername', $extendedUsername);
			}
			if($ldapUser->getOperation() != 'CREATE') {
				$ldapUser->setOperation('MODIFY');
			}
			$this->em->flush();
			$this->ldapProxy->syncLdapUserWithDBUser($ldapUser);
		}
		$this->em->persist($person);
		$this->em->flush();

	}
}