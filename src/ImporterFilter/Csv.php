<?php

namespace App\ImporterFilter;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\ImporterFilter\ImportedEntity\Teacher;
use App\ImporterFilter\ImportedEntity\Student;
use App\ImporterFilter\ImportedEntity\Group;

class Csv extends AbstractFilter
{
    static $name = "File CSV";
    static $internalName = "csv";
    static $parametersUi = ['filename' => ['title' => "File CSV (completo di percorso)", 'type' => TextType::class]];

    private $kernelRootDir;
    private $filename;

    public function __construct(string $kernelRootDir)
    {
        $this->kernelRootDir = $kernelRootDir;
    }

    public function setUri($uri)
    {
    }

    public function setSecretKey($key)
    {
    }

    public function setParameters($parameters)
    {
        parent::setParameters($parameters);


        if (substr($this->parameters['filename'], 0, 1) != '/') {
            $this->filename = $this->kernelRootDir . '/../' . $this->parameters['filename'];
        } else {
            $this->filename = $this->parameters['filename'];
        }


    }

    public function parseRemoteData()
    {
        $rows = file($this->filename);

        $header = explode(";", $rows[0]);
        $header = array_map('trim', $header);


        foreach ($rows as $k => $row) {

            if ($k == 0) {
                continue;
            }

            $v = array_combine($header, explode(";", $row));
            $v = array_map('trim', $v);

            $classe = trim($v['groupId']);
            $remove = array(
                '^',
                ',',
                '.',
                ':',
                '/',
                '\\',
                ',',
                '=',
                '+',
                '<',
                '>',
                ';',
                '"',
                '#',
                "'",
                '(',
                ')',
                "'",
                "\x00",
                '?',
                '.',
                '-',
                '!'
            );
            $classe = str_replace($remove, '', $classe);

            if (trim(strtolower($v['ID'])) == '') {
                continue;
            }
            if (strlen(trim($classe)) == 0) {
                continue;
            }
            $id = $v['ID'];
            $idClasse = md5(strtolower($classe));

            if ($v['type'] == 'studente') {
                $this->students[$id] = new Student((int)$id, strtolower($v['fiscalcode']), ucwords(strtolower($v['firstname'])), ucwords(strtolower($v['lastname'])), $idClasse, strtolower($v['email']), strtolower($v['username']), strtolower($v['password']));
                $this->groups[$idClasse] = new Group($idClasse, $classe, 0);
            }
            if ($v['type'] == 'docente') {
                $this->teachers[$id] = new Teacher((int)$id, strtolower($v['fiscalcode']), ucwords(strtolower($v['firstname'])), ucwords(strtolower($v['lastname'])), strtolower($v['email']), strtolower($v['username']), strtolower($v['password']));
            }


        }


    }
}
