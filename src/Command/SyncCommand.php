<?php

namespace App\Command;

use App\Manager\LdapProxy;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use App\Manager\GoogleAppManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SyncCommand extends Command
{
    protected $container;
    protected $ldapProxy;

    public function __construct(ContainerInterface $container,  LdapProxy $ldapProxy)
    {
        $this->ldapProxy = $ldapProxy;
        $this->container=$container;
        parent::__construct();
    }


    protected function configure()
    {
        $this->setName('sync')
            ->setDescription('Sync DB and Ldap and viceversa')
            ->addOption(
                'from',
                null,
                InputOption::VALUE_REQUIRED,
                'Sorgente della sincronizzazione?',
                'DB'
            )
            ->addOption(
                'to',
                null,
                InputOption::VALUE_REQUIRED,
                'Destinazione della sincronizzazione?',
                'LDAP'
            )
            ->addOption(
                'useMQ',
                null,
                InputOption::VALUE_NONE,
                'Usa RabbitMQ'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->ldapProxy->connect();

        $from = $input->getOption('from');
        $to = $input->getOption('to');

        $useMQ = $input->getOption('useMQ');

        $output->writeln('Sync from: <info>'.$from.'</info>');
        $output->writeln('       to: <info>'.$to.'</info>');

        if ($useMQ) {
            $output->writeln('Using RabbitMQ');
        } else {
            $output->writeln('Not using RabbitMQ');
        }

        $syncDirection = $from . $to;

        if (strtolower($syncDirection) == 'dbldap') {
            if ($useMQ) {
                $msg = array('command' => 'syncLDAPfromDB', 'parameters' => array());
                $client = $this->container->get('old_sound_rabbit_mq.ldap_service_producer');
                $client->publish(serialize($msg));
            } else {

                $this->ldapProxy->syncLDAPfromDB();
            }
        } elseif (strtolower($syncDirection) == 'ldapdb') {
            if ($useMQ) {
                $msg = array('command' => 'purgeAndPopulate', 'parameters' => array());
                $client = $this->container->get('old_sound_rabbit_mq.ldap_service_producer');
                $client->publish(serialize($msg));
            } else {
                $this->ldapProxy->purgeAndPopulateAll();
            }
        } else {
            throw(\Exception);
        }


    }

}