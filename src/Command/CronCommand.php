<?php
namespace App\Command;

use App\Manager\AppleSchoolManager;
use App\Manager\ConfigurationManager;
use App\Manager\LdapProxy;
use App\Manager\PersonsAndGroups;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Entity\Provider;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CronCommand extends Command
{
	/**
     * @var \App\Manager\PersonsAndGroups
     */
    private $personsAndGroups;
	/**
     * @var \App\Manager\LdapProxy
     */
    private $ldapProxy;
	/**
	 * @var AppleSchoolManager
	 */
    private $appleSchool;

    private $configurationManager;
    private $em;
    protected $logger;
    protected $output;
    protected $container;


    public function __construct(ContainerInterface $container, LoggerInterface $logger,EntityManagerInterface $em, ConfigurationManager $configurationManager, PersonsAndGroups $personsAndGroups, LdapProxy $ldapProxy, AppleSchoolManager $appleSchoolManager)
    {
        $this->logger = $logger;
        $this->em = $em;
        $this->configurationManager = $configurationManager;
        $this->personsAndGroups =$personsAndGroups;
        $this->ldapProxy = $ldapProxy;
        $this->appleSchool = $appleSchoolManager;
        $this->container=$container;
        parent::__construct();
    }


    protected function configure()
    {
        $this->setName('cron')
            ->setDescription('Importing and parsing');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->ldapProxy->connect();

        $this->output = $output;

        $providers = $this->em->getRepository('App:Provider')->findBy(array('active' => true));
        foreach ($providers as $provider) {
            if (!$provider instanceof Provider || !$provider->getFilter()) {
                continue;
            }

            $output->writeln("<info>Start Importing data from {$provider->getName()}</info>");
            $this->logger->info("Start Importing data from {$provider->getName()}");
            $filter = clone $this->container->get($provider->getFilter());
            $filter->setParameters($provider->getFilterData());
            try {
                $filter->parseRemoteData();
            } catch(\ErrorException $e) {
                $output->writeln("Errore nel parsing del provider {$provider->getName()}: ".$e->getMessage());
                $this->logger->error("Errore nel parsing del provider {$provider->getName()}: ".$e->getMessage());
                continue;
            }
            $this->personsAndGroups->importElements($filter, $provider);
            $output->writeln("<info>Ended Importing data from {$provider->getName()}</info>");
            $this->logger->info("Ended Importing data from {$provider->getName()}");
        }
        $this->printAndLogInfo('purgeAndPopulateAll');
        $this->ldapProxy->purgeAndPopulateAll();
        $this->printAndLogInfo('checkLdapGroups');
        $this->personsAndGroups->checkLdapGroups();
        $this->printAndLogInfo('checkLdapOUs');
        $this->personsAndGroups->checkLdapOUs();
        $this->printAndLogInfo('checkLdapUsers');
        $this->personsAndGroups->checkLdapUsers();
        $this->printAndLogInfo('checkLdapUsersMembership');
        $this->personsAndGroups->checkLdapUsersMembership();
        $this->printAndLogInfo('checkLdapUsersOUMembership');
        $this->personsAndGroups->checkLdapUsersOUMembership();

        $this->printAndLogInfo('syncLDAPfromDB</info> via RabbitMQ');
        $msg = array('command' => 'syncLDAPfromDB', 'parameters' => array());
        $client = $this->container->get('old_sound_rabbit_mq.ldap_service_producer');
        $client->publish(serialize($msg));

        $this->printAndLogInfo('checkGoogleAppsGroups via RabbitMQ');
        $this->personsAndGroups->checkGoogleAppsGroups();
        $this->printAndLogInfo('checkGoogleAppsOUs via RabbitMQ');
        $this->personsAndGroups->checkGoogleAppsOUs();
        $this->printAndLogInfo('checkGoogleAppsUsers via RabbitMQ');
        $this->personsAndGroups->checkGoogleAppsUsers();
        $this->printAndLogInfo('checkGoogleAppsUsersMembershipToGroup via RabbitMQ');
        $this->personsAndGroups->checkGoogleAppsUsersMembershipToGroup();
        $this->printAndLogInfo('checkGoogleAppsUsersMembershipToOU via RabbitMQ');
        $this->personsAndGroups->checkGoogleAppsUsersMembershipToOU();

		$this->printAndLogInfo('appleSchool generateAndUploadAllCsvFiles');
		$this->appleSchool->generateAndUploadAllCsvFiles();

//        $this->printAndLogInfo('syncLDAPfromDB');
//        $this->ldapProxy->syncLDAPfromDB();

        $this->printAndLogInfo("All done!");
    }
    
    protected function printAndLogInfo($message) {
        $this->output->writeln('<info>'.$message.'</info>');
        $this->logger->info("CRON:".$message);
    }
}