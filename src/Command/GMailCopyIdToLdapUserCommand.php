<?php

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Entity\Provider;
use App\Entity\Student;
use App\LdapTool;
use adLDAP\adLDAP;
use App\Manager\GoogleApps;

class GMailCopyIdToLdapUserCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('gmail:copy-id-to-ldap-user')
            ->setDescription('Copy distinguished id from LdapUser entity to gmail (using email as search criteria)');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $em = $this->getContainer()->get('doctrine')->getManager();
        $logger = $this->getContainer()->get('logger');

        $ldapUsers = $em->getRepository('App:LdapUser')->findAll();


        foreach ($ldapUsers as $ldapUser) {
            $distinguishedId = $ldapUser->getDistinguishedId();
            $student = $em->getRepository('App:Student')->find($distinguishedId);

            if (!$student) {
                $teacher = $em->getRepository('App:Teacher')->find($distinguishedId);
                $provider = $teacher->getProvider();
                $type = 'Teacher';
            } else {
                $provider = $student->getProvider();
                $type = 'Student';
            }

            if ($provider->getStudentGoogleAppDomain()) {

                if (!$ldapUser->getEmail()) {
                    $output->writeln("<info>$type - " . $ldapUser->getUsername() . "-> Skip.</info>");
                    continue;
                }
                $output->writeln("<info>$type - " . $ldapUser->getUsername() . " -> ". $ldapUser->getEmail() . "</info>");
                if ($ldapUser->getGoogleId()){
                    $output->writeln("<info>        > googleId: " . $ldapUser->getGoogleId()  . "</info>");
                    continue;
                }
                $googleApps = new GoogleApps(
                    $provider->getStudentGoogleAppDomain(),
                    $provider->getStudentGoogleAppClientId(),
                    $provider->getStudentGoogleAppclientSecret(),
                    $provider->getStudentGoogleAppOUPath(),
                    '/var/www/igroove/src',
                    $logger
                );
                $gmailUser=$googleApps->getUser($ldapUser->getEmail());
if ($gmailUser) {
    $ldapUser->setGoogleId($gmailUser['id']);
    $ldapUser->setOperation('MODIFY');

    $output->writeln("<comment>        > googleId: " . $ldapUser->getGoogleId() . "</comment>");

    $em->flush();
} else {
    $output->writeln("<error>        > Not found!</error>");
}

            }

        }
    }


}