<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class GuestSmsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('lastname', null, array('label' => 'Cognome:'))
                ->add('firstname', null, array('label' => 'Nome:'))
                ->add('sms', null, array('label' => 'Numero di telefono (+39):'))
        ;
    }

    public function getName()
    {
        return 'zen_igroovebundle_guest_smstype';
    }

}
