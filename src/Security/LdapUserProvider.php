<?php

namespace App\Security;

use Symfony\Component\Ldap\Entry;
use Symfony\Component\Security\Core\User\LdapUserProvider as SymfonyLdapUserProvider;
use Symfony\Component\Security\Core\User\User;

/**
 * Handles the mapping of ldap groups to security roles.
 * Class LdapUserProvider
 * @package App\Security
 */
class LdapUserProvider extends SymfonyLdapUserProvider
{

    /** @var string extracts group name from dn string */
    private $groupNameRegExp = '/^CN=(?P<group>[^,]+),.*$/i'; // You might want to change it to match your ldap server






    protected function loadUser($username, Entry $entry)
    {
        $password = null;


        $roles = ['ROLE_USER']; // Actually we should be using $this->defaultRoles, but it's private. Has to be redesigned.


        if (!$entry->hasAttribute('memberOf')) { // Check if the entry has attribute with the group
            return new User($username, $password, $roles);
        }
        foreach ($entry->getAttribute('memberOf') as $groupLine) { // Iterate through each group entry line


            $groupName = strtoupper($this->getGroupName($groupLine)); // Extract and normalize the group name fron the line
            $re = "/[^a-zA-Z0-9 ]/";
            $groupName = preg_replace($re, '', $groupName);
            $groupName = str_replace('  ', '_', $groupName);
            $groupName = str_replace(' ', '_', $groupName);

            $roles[] = 'ROLE_' . $groupName; // Map the group to the role the user will have
        }

        return new User($username, $password, $roles); // Create and return the user object
    }

    /**
     * Get the group name from the DN
     * @param string $dn
     * @return string
     */
    private function getGroupName($dn)
    {
        $matches = [];
        return preg_match($this->groupNameRegExp, $dn, $matches) ? $matches['group'] : '';
    }
}