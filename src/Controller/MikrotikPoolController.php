<?php

namespace App\Controller;

use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\MikrotikPool;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * MikrotikPool controller.
 *
 * @Route("/mikrotik/pool")
 */
class MikrotikPoolController extends BaseAbstractController
{


    /**
     * Lists all MikrotikPool entities.
     *
     * @Route("/", name="mikrotik_pool")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("mikrotik_pool/index.html.twig")
     */
    public function indexAction(PaginatorInterface $paginator, Request $request)
    {
        $client = $this->container->get('old_sound_rabbit_mq.mikrotik_service_producer');
        $msg = array('command' => 'addPools', 'parameters' => array());
        $client->publish(serialize($msg));

        $queryString = $request->get('queryString', false);
        $q = '%'.$queryString.'%';
        $em = $this->getDoctrine()->getManager();
        if ($queryString) {
            $query = $em->createQuery(
                'SELECT l FROM App:MikrotikPool l WHERE l.nome  LIKE :q ORDER BY l.nome'
            )
                ->setParameter('q', $q);
        } else {
            $query = $em->createQuery('SELECT l FROM App:MikrotikPool l  ORDER BY l.nome');
        }
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1), 25
        );

        return array(
            'pagination' => $pagination,
        );
    }

    /**
     * Creates a new MikrotikPool entity.
     *
     * @Route("/create", name="mikrotik_pool_create")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST")
     * @Template("mikrotik_pool/new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new MikrotikPool();

        $form = $this->createForm('App\Form\MikrotikPoolType', $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);

            $notAssociatedDeviceIps = $em->getRepository('App:DeviceIp')->getAllNotAssociatedIpsInRange($entity->getIpStart(), $entity->getIpEnd())->toArray();
            $outOfRangeDeviceIps = $em->getRepository('App:DeviceIp')->getAllAssociatedIpsOutOfRange($entity, $entity->getIpStart(), $entity->getIpEnd())->toArray();

            foreach ($notAssociatedDeviceIps as $notAssociatedDeviceIp) {
                $notAssociatedDeviceIp->setMikrotikPool($entity);
                $em->persist($notAssociatedDeviceIp);
            }

            foreach ($outOfRangeDeviceIps as $outOfRangeDeviceIp) {
                $outOfRangeDeviceIp->setMikrotikPool(null);
                $em->persist($outOfRangeDeviceIp);
            }

            $em->flush();

            return $this->redirect($this->generateUrl('mikrotik_pool'));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new MikrotikPool entity.
     *
     * @Route("/new", name="mikrotik_pool_new")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("mikrotik_pool/new.html.twig")
     */
    public function newAction()
    {
        $entity = new MikrotikPool();
        $form = $this->createForm('App\Form\MikrotikPoolType', $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing MikrotikPool entity.
     *
     * @Route("/{id}/edit", name="mikrotik_pool_edit")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("mikrotik_pool/edit.html.twig")
     */
    public function editAction(Request $request, MikrotikPool $mikrotikPool)
    {
        if (!$mikrotikPool) {
            throw $this->createNotFoundException('Unable to find Mikrotik entity.');
        }

        $editForm = $this->createForm('App\Form\MikrotikPoolType', $mikrotikPool);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $notAssociatedDeviceIps = $em->getRepository('App:DeviceIp')->getAllNotAssociatedIpsInRange($mikrotikPool->getIpStart(), $mikrotikPool->getIpEnd())->toArray();
            $outOfRangeDeviceIps = $em->getRepository('App:DeviceIp')->getAllAssociatedIpsOutOfRange($mikrotikPool, $mikrotikPool->getIpStart(), $mikrotikPool->getIpEnd())->toArray();

            foreach ($notAssociatedDeviceIps as $notAssociatedDeviceIp) {
                $notAssociatedDeviceIp->setMikrotikPool($mikrotikPool);
                $em->persist($notAssociatedDeviceIp);
            }

            foreach ($outOfRangeDeviceIps as $outOfRangeDeviceIp) {
                $outOfRangeDeviceIp->setMikrotikPool(null);
                $em->persist($outOfRangeDeviceIp);
            }

            $em->flush();

            return $this->redirect($this->generateUrl('mikrotik_pool', array()));
        }

        return array(
            'entity' => $mikrotikPool,
            'edit_form' => $editForm->createView(),
        );
    }

    /**
     * Deletes a MikrotikPool entity.
     *
     * @Route("/{id}", name="mikrotik_pool_delete")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction(Request $request, MikrotikPool $mikrotikPool)
    {
        if (!$mikrotikPool) {
            throw $this->createNotFoundException('Unable to find Mikrotik entity.');
        }
        $em = $this->getDoctrine()->getManager();

        foreach ($mikrotikPool->getDeviceIps() as $deviceIp) {
            $deviceIp->setMikrotikPool(null);
            $em->persist($deviceIp);
        }

        $em->remove($mikrotikPool);
        $em->flush();

        return $this->redirect($this->generateUrl('mikrotik_pool'));
    }

    /**
     * Check first free ip for in a pool.
     *
     * @Route("/{id}/checkFreeIp", name="mikrotik_pool_check_free_ip")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function checkFreeIpAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('App:MikrotikPool')->find($id);

        if (!$entity) {
            return new Response(json_encode(['error' => 'Unable to find the pool you specified']), 404);
        }

        $selectedIp = null;
        $lastIp = $entity->getIpStart();
        foreach ($entity->getDeviceIps() as $ip) {
            if ($lastIp < $ip->getRawIp()) {
                $selectedIp = $lastIp;
                break;
            }

            $lastIp = $ip->getRawIp() + 1;
        }

        if (null === $selectedIp && $lastIp <= $entity->getIpEnd()) {
            $selectedIp = $lastIp;
        }

        if (null === $selectedIp) {
            return new Response(json_encode(['error' => "No more ip in the range {$entity->getDottedIpStart()} - {$entity->getDottedIpEnd()} of the pool {$entity->getNome()}"]));
        }

        return new Response(json_encode(['ip' => long2ip($selectedIp)]));
    }
}
