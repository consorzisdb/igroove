<?php

namespace App\Controller;

use App\Manager\CoaManager;
use App\Manager\ConfigurationManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Group;
use App\Entity\InternetOpen;
use App\Entity\MikrotikList;
use App\Entity\Provider;
use App\Entity\Sector;
use App\Entity\Student;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class InternetController extends BaseAbstractController
{


    /**
     * Enable internet for speciefied group
     *
     * @Route("/internet/{providerId}/{groupId}/on", name="internetOn")
     * @Secure(roles="ROLE_TEACHER")
     */
    public function internetOnAction(ConfigurationManager $configurationManager, EntityManagerInterface $em,Request $request,$providerId, $groupId)
    {
        $active_directory_generated_group_prefix = $configurationManager->getActiveDirectoryGeneratedGroupPrefix();
        $ldapGroupRepository = $em->getRepository('App:LdapGroup');

        $group = $em->getRepository("App:Group")->find($groupId);
        if(!$group instanceof Group) {
            throw $this->createNotFoundException('Non è stato possibile trovare il gruppo indicato.');
        }

        $ldapGroupName = $active_directory_generated_group_prefix . $ldapGroupRepository::ldapEscape($group->getName());
        $currentUserName = $this->getCurrentUserUsername();
        $closeAt = new \DateTime($request->get('timeToSet', false));
        $permitPersonalDevice = (bool)$request->get('personalDevice', false);

        $currentEntity = $em->getRepository("App:InternetOpen")->findOneBy(['account' => $ldapGroupName, 'type' => 'group']);
        if($currentEntity instanceof InternetOpen) {
            $currentEntity->setCloseAt($closeAt);
            $currentEntity->setActivedBy($currentUserName);
            $currentEntity->setPermitPersonalDevices($permitPersonalDevice);

            $internetLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix . "InternetAccess");
            $personalDeviceLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix . "PersonalDeviceAccess");
            $modifiedLdapGroup = false;

            if(!$internetLdapGroup->memberExists('group', $ldapGroupName)) {
                $internetLdapGroup->addMember('group', $ldapGroupName);
                $modifiedLdapGroup = true;
            }

            if($permitPersonalDevice && !$personalDeviceLdapGroup->memberExists('group', $ldapGroupName)) {
                $personalDeviceLdapGroup->addMember('group', $ldapGroupName);
                $modifiedLdapGroup = true;
            } elseif (!$permitPersonalDevice && $personalDeviceLdapGroup->memberExists('group', $ldapGroupName)) {
                $personalDeviceLdapGroup->removeMember('group', $ldapGroupName);
                $modifiedLdapGroup = true;
            }

            $em->flush();

            if($modifiedLdapGroup) {
                $msg = array('command' => 'syncInternetAccessLdapGroup', 'parameters' => array());
                $client = $this->container->get('old_sound_rabbit_mq.ldap_service_producer');
                $client->publish(serialize($msg));
            }

            $this->logAction("internetOn-group","ha esteso {$group}".($permitPersonalDevice?" con i dispositivi personali":""));
        } else {
            $internetOpen = new InternetOpen();
            $internetOpen->setType('group');
            $internetOpen->setAccount($ldapGroupName);
            $internetOpen->setCloseAt($closeAt);
            $internetOpen->setActivedBy($currentUserName);
            $internetOpen->setActivationCompleated(FALSE);
            $internetOpen->setPermitPersonalDevices($permitPersonalDevice);
            $em->persist($internetOpen);

            $internetLdapGroup = $em->getRepository('App:LdapGroup')->find($active_directory_generated_group_prefix . "InternetAccess");
            $personalDeviceLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix . "PersonalDeviceAccess");

            $internetLdapGroup->addMember('group', $ldapGroupName);

            if($permitPersonalDevice) {
                $personalDeviceLdapGroup->addMember('group', $ldapGroupName);
            }

            $em->flush();

            $msg = array('command' => 'syncInternetAccessLdapGroup', 'parameters' => array());
            $client = $this->container->get('old_sound_rabbit_mq.ldap_service_producer');
            $client->publish(serialize($msg));
            $this->logAction("internetOn-group","ha abilitato {$group}".($permitPersonalDevice?" con i dispositivi personali":""));
        }

        return $this->redirect(
            $this->generateUrl(
                'list_groups_in_provider',
                array(
                    'providerId' => $providerId,
                    'sectorId' =>$request->get('sectorId',false),
                    'activatedGroup' => $groupId
                )
            )
        );
    }

    /**
     * Disable internet for specified group
     *
     * @Route("/internet/{providerId}/{groupId}/off", name="internetOff")
     * @Secure(roles="ROLE_TEACHER")
     */
    public function internetOffAction(ConfigurationManager $configurationManager, EntityManagerInterface $em,Request $request,   CoaManager $coaManager,$providerId, $groupId)
    {
        $active_directory_generated_group_prefix = $configurationManager->getActiveDirectoryGeneratedGroupPrefix();
        $ldapGroupRepository = $em->getRepository('App:LdapGroup');

        $group = $em->getRepository("App:Group")->find($groupId);
        if(!$group instanceof Group) {
            throw $this->createNotFoundException('Non è stato possibile trovare il gruppo indicato.');
        }

        $em->getRepository('App:InternetOpen')->removeGroup($active_directory_generated_group_prefix . $ldapGroupRepository::ldapEscape($group->getName()));

        $internetLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix . "InternetAccess");
        $personalDeviceLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix . "PersonalDeviceAccess");

        $internetLdapGroup->removeMember('group',  $active_directory_generated_group_prefix .$group->getName());


        $personalDeviceLdapGroup->removeMember('group',  $active_directory_generated_group_prefix .$group->getName());
        $em->flush();

        //@todo disattivare anche gli studenti singoli?

        $msg = array('command' => 'syncInternetAccessLdapGroup', 'parameters' => array());
        $client = $this->container->get('old_sound_rabbit_mq.ldap_service_producer');
        $client->publish(serialize($msg));

        $this->removeCoaFromGroup($em,$coaManager,$active_directory_generated_group_prefix .$group->getName());
        $this->logAction("internetOff-group","ha disabilitato {$group}");

        return $this->redirect(
            $this->generateUrl(
                'list_groups_in_provider',
                array(
                    'providerId' => $providerId,
                    'sectorId' =>$request->get('sectorId',false)
                )
            )
        );
    }

    /**
     * Enable internet for speciefied student
     *
     * @Route("/internet/{providerId}/{groupId}/{studentId}/on", name="internetOnUser")
     * @Secure(roles="ROLE_TEACHER")
     */
    public function internetOnUserAction(ConfigurationManager $configurationManager, EntityManagerInterface $em,Request $request,$providerId, $groupId, $studentId)
    {
        $timeToSet = $request->get('timeToSet', false);
        $closeAt = new \DateTime($timeToSet);
        $currentUserName = $this->getCurrentUserUsername();
        $ldapGroupRepository = $em->getRepository('App:LdapGroup');
        $permitPersonalDevice = $request->get('personalDevice', false) == "1";

        $student = $em->getRepository("App:Student")->find($studentId);
        if(!$student instanceof Student) {
            throw $this->createNotFoundException('Non è stato possibile trovare lo studente indicato.');
        }

        $active_directory_generated_group_prefix = $configurationManager->getActiveDirectoryGeneratedGroupPrefix();

        $currentEntity = $em->getRepository("App:InternetOpen")->findOneBy(['account' => $student->getUsername(), 'type' => 'user']);
        if($currentEntity instanceof InternetOpen) {
            $currentEntity->setCloseAt($closeAt);
            $currentEntity->setActivedBy($currentUserName);
            $currentEntity->setPermitPersonalDevices($permitPersonalDevice);

            $modifiedLdapGroup = false;
            $internetLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix . "InternetAccess");
            $personalDeviceLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix . "PersonalDeviceAccess");

            if(!$internetLdapGroup->memberExists('user', $student->getUsername())) {
                $internetLdapGroup->addMember('user', $student->getUsername());
                $modifiedLdapGroup = true;
            }

            if($permitPersonalDevice && !$personalDeviceLdapGroup->memberExists('user', $student->getUsername())) {
                $personalDeviceLdapGroup->addMember('user', $student->getUsername());
                $modifiedLdapGroup = true;
            } elseif (!$permitPersonalDevice && $personalDeviceLdapGroup->memberExists('user', $student->getUsername())) {
                $personalDeviceLdapGroup->removeMember('user', $student->getUsername());
                $modifiedLdapGroup = true;
            }

            $em->flush();

            if($modifiedLdapGroup) {
                $msg = array('command' => 'syncInternetAccessLdapGroup', 'parameters' => array());
                $client = $this->container->get('old_sound_rabbit_mq.ldap_service_producer');
                $client->publish(serialize($msg));
            }

            $this->logAction("internetOn-user","ha esteso {$student->getUsername()}");
        } else {
            $internetOpen = new InternetOpen();
            $internetOpen->setType('user');
            $internetOpen->setAccount($student->getUsername());
            $internetOpen->setCloseAt($closeAt);
            $internetOpen->setActivedBy($currentUserName);
            $internetOpen->setActivationCompleated(FALSE);
            $internetOpen->setPermitPersonalDevices($permitPersonalDevice);
            $em->persist($internetOpen);

            $internetLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix . "InternetAccess");
            $personalDeviceLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix . "PersonalDeviceAccess");

            $internetLdapGroup->addMember('user', $student->getUsername());
            if($permitPersonalDevice){
                $personalDeviceLdapGroup->addMember('user', $student->getUsername());
            }

            $em->flush();

            $msg = array('command' => 'syncInternetAccessLdapGroup', 'parameters' => array());
            $client = $this->container->get('old_sound_rabbit_mq.ldap_service_producer');
            $client->publish(serialize($msg));

            $this->logAction("internetOn-user","ha abilitato {$student->getUsername()}");
        }

        return $this->redirect(
            $this->generateUrl(
                'list_students_in_group',
                array(
                    'providerId' => $providerId,
                    'groupId' => $groupId,
                    'activatedUser' => $studentId
                )
            )
        );
    }

    /**
     * Disable internet for specified student
     *
     * @Route("/internet/{providerId}/{groupId}/{studentId}/off", name="internetOffUser")
     * @Secure(roles="ROLE_TEACHER")
     */
    public function internetOffUserAction(ConfigurationManager $configurationManager, EntityManagerInterface $em,   CoaManager $coaManager,$providerId, $groupId, $studentId)
    {
        $student = $em->getRepository("App:Student")->find($studentId);
        if(!$student instanceof Student) {
            throw $this->createNotFoundException('Non è stato possibile trovare lo studente indicato.');
        }

        $em->getRepository('App:InternetOpen')->removeUser($student->getUsername());

        $active_directory_generated_group_prefix = $configurationManager->getActiveDirectoryGeneratedGroupPrefix();

        $ldapGroupRepository = $em->getRepository('App:LdapGroup');
        $internetLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix . "InternetAccess");
        $personalDeviceLdapGroup = $ldapGroupRepository->find($active_directory_generated_group_prefix . "PersonalDeviceAccess");

        $internetLdapGroup->removeMember('user', $student->getUsername());
        $personalDeviceLdapGroup->removeMember('user', $student->getUsername());
        $em->flush();

        $msg = array('command' => 'syncInternetAccessLdapGroup', 'parameters' => array());
        $client = $this->container->get('old_sound_rabbit_mq.ldap_service_producer');
        $client->publish(serialize($msg));
        $coaManager->KickOffUser( $student->getUsername());

        $this->logAction("internetOff-user","ha disabilitato {$student->getUsername()}");

        return $this->redirect(
            $this->generateUrl(
                'list_students_in_group',
                array(
                    'providerId' => $providerId,
                    'groupId' => $groupId,
                )
            )
        );
    }

    /**
     * Enable internet for specified mikrotik ip list
     *
     * @Route("/mikrotik/list/internet/{mikrotikList}/on", name="internetOnIpList")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function internetOnIpList(EntityManagerInterface $em,Request $request,MikrotikList $mikrotikList)
    {
        if(!$mikrotikList instanceof MikrotikList) {
            throw $this->createNotFoundException('Non è stato possibile trovare la lista indicata.');
        }

        $timeToSet = $request->get('timeToSet', false);
        $closeAt = new \DateTime($timeToSet);
        $currentUserName = $this->getCurrentUserUsername();

        $currentEntity = $em->getRepository("App:InternetOpen")->findOneBy(['account' => $mikrotikList->getId(), 'type' => 'ipList']);
        if($currentEntity instanceof InternetOpen) {
            $currentEntity->setCloseAt($closeAt);
            $currentEntity->setActivedBy($currentUserName);
            $em->flush();

            $this->logAction("internetOn-ipList","ha esteso {$mikrotikList->getNome()}");
        } else {
            $internetOpen = new InternetOpen();
            $internetOpen->setType('ipList');
            $internetOpen->setAccount($mikrotikList->getId());
            $internetOpen->setCloseAt($closeAt);
            $internetOpen->setActivedBy($currentUserName);
            $internetOpen->setActivationCompleated(FALSE);
            $em->persist($internetOpen);
            $em->flush();

            $this->logAction("internetOn-ipList","ha abilitato {$mikrotikList->getNome()}");
        }

        $msg = array('command' => 'addIpToBypassedList', 'parameters' => array());
        $mktclient = $this->container->get('old_sound_rabbit_mq.mikrotik_service_producer');
        $mktclient->publish(serialize($msg));

        return $this->redirect($this->generateUrl('mikrotik_list', []));
    }

    /**
     * Disable internet for specified mikrotik ip list
     *
     * @Route("/mikrotik/list/internet/{mikrotikListId}/off", name="internetOffIpList")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function internetOffIpList(EntityManagerInterface $em,$mikrotikListId)
    {
        $mikrotikList = $em->getRepository("App:MikrotikList")->find($mikrotikListId);
        if(!$mikrotikList instanceof MikrotikList) {
            throw $this->createNotFoundException('Non è stato possibile trovare la lista indicata.');
        }

        $internetOpen = $em->getRepository('App:InternetOpen')->findOneBy(['account' => $mikrotikListId, 'type' => "ipList"]);
        if(!$internetOpen instanceof InternetOpen) {
            return $this->redirect($this->generateUrl('mikrotik_list', []));
        }

        $em->remove($internetOpen);
        $em->flush();

        $this->logAction("internetOff-ipList","ha disabilitato {$mikrotikList->getNome()}");

        $msg = array('command' => 'addIpToBypassedList', 'parameters' => array());
        $mktclient = $this->container->get('old_sound_rabbit_mq.mikrotik_service_producer');
        $mktclient->publish(serialize($msg));

        return $this->redirect($this->generateUrl('mikrotik_list', []));
    }

    /**
     * @Route("/internet/{providerId}/{groupId}/isOn", name="internetIsOn")
     */
    public function internetIsOnAction(ConfigurationManager $configurationManager, EntityManagerInterface $em,$providerId, $groupId)
    {
        $active_directory_generated_group_prefix = $configurationManager->getActiveDirectoryGeneratedGroupPrefix();
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        $group = $em->getRepository('App:Group')->find($groupId);
        if(!$group instanceof Group) {
            $response->setContent(json_encode(["on" => null, 'activationCompleted' => null, 'error' => "Non è stato possibile trovare il gruppo indicato."]));
            return $response;
        }

        $ldapGroupName = $active_directory_generated_group_prefix . $em->getRepository('App:LdapGroup')::ldapEscape($group->getName());
        $internetOpen = $em->getRepository("App:InternetOpen")->findOneBy(['account' => $ldapGroupName, 'type' => 'group']);
        if(!$internetOpen instanceof InternetOpen) {
            $response->setContent(json_encode(["on" => false, 'activationCompleted' => null]));
        } else {
            $response->setContent(json_encode(["on" => true, 'activationCompleted' => $internetOpen->getActivationCompleated()]));
        }

        return $response;
    }

    /**
     * @Route("/internet/{providerId}/{groupId}/{studentId}/isOn", name="internetIsOnForUser")
     * @Secure(roles="ROLE_TEACHER")
     */
    public function internetIsOnForUserAction( EntityManagerInterface $em,$providerId, $groupId, $studentId)
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        $student = $em->getRepository("App:Student")->find($studentId);
        if(!$student instanceof Student) {
            $response->setContent(json_encode(["on" => null, 'activationCompleted' => null, 'error' => "Non è stato possibile trovare lo studente indicato."]));
            return $response;
        }

        $internetOpen = $em->getRepository("App:InternetOpen")->findOneBy(['account' => $student->getUsername(), 'type' => 'user']);
        if(!$internetOpen instanceof InternetOpen) {
            $response->setContent(json_encode(["on" => false, 'activationCompleted' => null]));
        } else {
            $response->setContent(json_encode(["on" => true, 'activationCompleted' => $internetOpen->getActivationCompleated()]));
        }

        return $response;
    }
    private function removeCoaFromGroup($em,$coaManager,$groupName)
    {
        $usernames = $em->getRepository('App:LdapGroup')
            ->getAllChildrenRecursiveUsers($groupName);
        foreach ($usernames as $username) {
            $coaManager->KickOffUser($username);
        }
    }
}
