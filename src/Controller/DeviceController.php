<?php

namespace App\Controller;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Device;
use App\Entity\MikrotikList;
use App\Form\DeviceType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\Manager\ConfigurationManager;

/**
 * Device controller.
 *
 */
class DeviceController extends BaseAbstractController
{

    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Lists all Device entities.
     *
     * @Route("/device", name="device")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function indexAction(PaginatorInterface $paginator,Request $request)
    {
        $filter=array();
        $queryString = $request->get('queryString', false);
        if ($queryString) {
            $filter['queryString']=$queryString;
        }
        $onlyActive = $request->get('onlyActive', 'off') == 'on' ? true : false;
        $filter['onlyActive']=$onlyActive;
        $mikrotikListId = $request->get('mikrotikListId', false);
        if (($mikrotikListId!==false) and ($mikrotikListId!=-1)){
            $filter['mikrotikListId']=$mikrotikListId;
        }

        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('App:Device')->getQueryFromFilter($filter);


        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),25
        );

        $mikrotikLists = $em->getRepository('App:MikrotikList')->findBy([], ['nome' => 'ASC']);

        $request->getSession()->set("macControllerLastQuery",$request->getQueryString());

        return array(
            'pagination' => $pagination,
            'filter' => $filter,
            'mikrotikLists'=>$mikrotikLists
        );
    }

    /**
     * Displays a form to create a new Device entity.
     *
     * @Route("/device/new", name="device_new")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Device();
        $entity->setActive(true);

        $qs = $this->getLastQueryForRedirect($request);

        if(!empty($qs) && isset($qs['mikrotikListId']) && $qs['mikrotikListId'] != "") {
            $mikrotikList = $em->getRepository('App:MikrotikList')->find($qs['mikrotikListId']);
            if($mikrotikList instanceof MikrotikList){
                $entity->setMikrotikList($mikrotikList);
            }
        }

        $form = $this->createForm('App\Form\DeviceType', $entity);

        $mikrotikPoolList = $em->getRepository('App:MikrotikPool')->findBy([],['nome' => 'ASC']);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'mikrotikPoolList' => $mikrotikPoolList,
            'redirect_qs' => $qs
        );
    }

    /**
     * Creates a new Device entity.
     *
     * @Route("/device/create", name="device_create")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("post")
     * @Template("Device/new.html.twig")
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Device();
        $form = $this->createForm('App\Form\DeviceType', $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $client = $this->container->get('old_sound_rabbit_mq.mikrotik_service_producer');
            $msg = array('command' => 'addMacToHospot', 'parameters' => array());
            $client->publish(serialize($msg));
            $msg = array('command' => 'addIpReservation', 'parameters' => array());
            $client->publish(serialize($msg));
            $msg = array('command' => 'addIpList', 'parameters' => array());
            $client->publish(serialize($msg));

            return $this->redirect($this->generateUrl('device', $this->getLastQueryForRedirect($request)));
        }
        $mikrotikPoolList = $em->getRepository('App:MikrotikPool')->findBy([],['nome' => 'ASC']);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'mikrotikPoolList' => $mikrotikPoolList,
            'redirect_qs' => $this->getLastQueryForRedirect($request)
        );
    }

    /**
     * Displays a form to edit an existing Device entity.
     *
     *
     * @Route("/device/{id}/edit", name="device_edit")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function editAction(Request $request, Device $device)
    {

        if (!$device) {
            throw $this->createNotFoundException('Unable to find Device entity.');
        }
        $em = $this->getDoctrine()->getManager();
        $mikrotikPoolList = $em->getRepository('App:MikrotikPool')->findBy([],['nome' => 'ASC']);

        $editForm = $this->createForm('App\Form\DeviceType', $device);

        return array(
            'entity' => $device,
            'edit_form' => $editForm->createView(),
            'mikrotikPoolList' => $mikrotikPoolList,
            'redirect_qs' => $this->getLastQueryForRedirect($request)
        );
    }

    /**
     * Edits an existing Device entity.
     *
     * @Route("/device/{id}/update", name="device_update")
     * @Method("post")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("Device/edit.html.twig")
     */
    public function updateAction(Request $request, Device $device)
    {
        $em = $this->getDoctrine()->getManager();


        if (!$device) {
            throw $this->createNotFoundException('Unable to find Device entity.');
        }

        $originalIps = clone $device->getIps();
        $editForm = $this->createForm('App\Form\DeviceType', $device);


        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            foreach ($originalIps as $deviceIp) {
                if (false === $device->getIps()->contains($deviceIp)) {
                    $em->remove($deviceIp);
                }
            }

            $em->persist($device);
            $em->flush();

            $client = $this->container->get('old_sound_rabbit_mq.mikrotik_service_producer');
            $msg = array('command' => 'addMacToHospot', 'parameters' => array());
            $client->publish(serialize($msg));
            $msg = array('command' => 'addIpReservation', 'parameters' => array());
            $client->publish(serialize($msg));
            $msg = array('command' => 'addIpList', 'parameters' => array());
            $client->publish(serialize($msg));

            return $this->redirect($this->generateUrl('device', $this->getLastQueryForRedirect($request)));
        }

        $mikrotikPoolList = $em->getRepository('App:MikrotikPool')->findBy([],['nome' => 'ASC']);

        return array(
            'entity' => $device,
            'edit_form' => $editForm->createView(),
            'mikrotikPoolList' => $mikrotikPoolList,
            'redirect_qs' => $this->getLastQueryForRedirect($request)
        );
    }

    /**
     * Deletes a Device entity.
     *
     * @Route("/device/{id}/delete", name="device_delete")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction(Request $request, Device $device)
    {

        if (!$device) {
            throw $this->createNotFoundException('Unable to find device entity.');
        }
        $em = $this->getDoctrine()->getManager();

        foreach ($device->getIps() as $deviceIp) {
            $em->remove($deviceIp);
        }

        $em->remove($device);
        $em->flush();

        $client = $this->container->get('old_sound_rabbit_mq.mikrotik_service_producer');
        $msg = array('command' => 'addMacToHospot', 'parameters' => array());
        $client->publish(serialize($msg));
        $msg = array('command' => 'addIpReservation', 'parameters' => array());
        $client->publish(serialize($msg));
        $msg = array('command' => 'addIpList', 'parameters' => array());
        $client->publish(serialize($msg));

        return $this->redirect($this->generateUrl('device', $this->getLastQueryForRedirect($request)));
    }

    /**
     * Last query searched in list (to go back to the last search)
     *
     * @return array
     */
    protected function getLastQueryForRedirect(Request $request) {
        $qs = [];
        if($request->getSession()->get("macControllerLastQuery","") != "") {
            parse_str($request->getSession()->get("macControllerLastQuery"), $qs);
        }

        return $qs;
    }
}
