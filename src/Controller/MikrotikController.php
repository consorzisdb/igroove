<?php

namespace App\Controller;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use App\Entity\Mikrotik;
use App\Form\MikrotikType;
use App\Manager\ConfigurationManager;
use App\Manager\MikrotikManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Mikrotik controller.
 *
 * @Route("/config/mikrotik")
 */
class MikrotikController extends BaseAbstractController
{


    /**
     * Refresh all Mikrotik setups.
     *
     * @Route("/refresh", name="mikrotik_refresh")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function refreshAction()
    {
        $client = $this->get('old_sound_rabbit_mq.mikrotik_service_producer');
        $msg = array('command' => 'addMacToHospot', 'parameters' => array());
        $client->publish(serialize($msg));
        $msg = array('command' => 'addIpReservation', 'parameters' => array());
        $client->publish(serialize($msg));
        $msg = array('command' => 'addIpList', 'parameters' => array());
        $client->publish(serialize($msg));
        $msg = array('command' => 'addPools', 'parameters' => array());
        $client->publish(serialize($msg));
        $response = new Response(json_encode([]));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Lists all MikrotikList entities.
     *
     * @Route("/", name="mikrotik")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(PaginatorInterface $paginator, Request $request)
    {
        $queryString = $request->get('queryString', false);
        $q = '%' . $queryString . '%';
        $em = $this->getDoctrine()->getManager();
        if ($queryString) {
            $query = $em->createQuery(
                "SELECT l FROM App:Mikrotik l WHERE l.ip  LIKE :q ORDER BY l.ip"
            )
                ->setParameter('q', $q);
        } else {
            $query = $em->createQuery("SELECT l FROM App:Mikrotik l  ORDER BY l.ip");
        }
        //$paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),25
        );


        return array(
            'pagination' => $pagination
        );
    }

    /**
     * Displays a form to create a new Guest entity.
     *
     * @Route("/new", name="config_mikrotik_new")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function newAction()
    {

        $entity = new Mikrotik();
        $form = $this->createForm('App\Form\MikrotikType', $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a new Guest entity.
     *
     * @Route("/create", name="config_mikrotik_create")
     * @Method("post")
     * @Template("mikrotik/new.html.twig")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function createAction(Request $request)
    {
        $entity = new Mikrotik();
        $form = $this->createForm('App\Form\MikrotikType', $entity);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('mikrotik', array()));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }





    /**
     * Displays a form to edit an existing Mikrotik entity.
     *
     * @Route("/{id}/edit", name="config_mikrotik_edit")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function editAction(request $request, Mikrotik $mikrotik)
    {

        if (!$mikrotik) {
            throw $this->createNotFoundException('Unable to find Mikrotik entity.');
        }

        $editForm = $this->createForm('App\Form\MikrotikType', $mikrotik);


        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $this->getDoctrine()->getManager()->flush();

            return $this->redirect($this->generateUrl('mikrotik', array()));
        }

        return array(
            'entity' => $mikrotik,
            'edit_form' => $editForm->createView()
        );
    }


    /**
     * Deletes a Mikrotik entity.
     *
     * @Route("/{id}/delete", name="config_mikrotik_delete")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction(Request $request, Mikrotik $mikrotik)
    {



            if (!$mikrotik) {
                throw $this->createNotFoundException('Unable to find Mikrotik entity.');
            }
        $em = $this->getDoctrine()->getManager();
            $em->remove($mikrotik);
            $em->flush();


        return $this->redirect($this->generateUrl('mikrotik'));
    }



    /**
     * Get all dhcp servers name from the Mikrotik
     *
     * @Route("/{id}/getDhcpServersName", name="config_mikrotik_get_dhcp_servers_name")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function getDhcpServerName(MikrotikManager $mikrotikManager, Mikrotik $mikrotik) {

        if (!$mikrotik || $mikrotik->getIp() == "") {
            return new Response(json_encode(['error' => 'Unable to find the mikrotik you specified']), 404);
        }


        return new Response(json_encode(['names' => $mikrotikManager->getDhcpServersName($mikrotik->getIp())]));
    }
}
