<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Radreply
 *
 * @ORM\Table(name="nas", indexes={@ORM\Index(name="nas_nasname", columns={"nasname"})})
 * @ORM\Entity
 */
class RadNas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nasname", type="string", length=128, nullable=false)
     */
    private $nasname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=32, nullable=true)
     */
    private $shortname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=30, nullable=false)
     */
    private $type = 'other';

    /**
     * @var string
     *
     * @ORM\Column(name="ports", type="integer", length=5, nullable=true)
     */
    private $ports;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="secret", type="string", length=60, nullable=false)
	 */
	private $secret = 'secret';

	/**
	 * @var string
	 *
	 * @ORM\Column(name="server", type="string", length=60, nullable=true)
	 */
	private $server;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="community", type="string", length=50, nullable=true)
	 */
	private $community;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=200, nullable=true)
	 */
	private $description = "RADIUS Client";



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * @return string
	 */
	public function getNasname(): string {
		return $this->nasname;
	}

	/**
	 * @param string $nasname
	 * @return RadNas
	 */
	public function setNasname(string $nasname): RadNas {
		$this->nasname = $nasname;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getShortname(): string {
		return $this->shortname;
	}

	/**
	 * @param string $shortname
	 * @return RadNas
	 */
	public function setShortname(string $shortname): RadNas {
		$this->shortname = $shortname;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getType(): string {
		return $this->type;
	}

	/**
	 * @param string $type
	 * @return RadNas
	 */
	public function setType(string $type): RadNas {
		$this->type = $type;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getPorts(): string {
		return $this->ports;
	}

	/**
	 * @param string $ports
	 * @return RadNas
	 */
	public function setPorts(string $ports): RadNas {
		$this->ports = $ports;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getSecret(): string {
		return $this->secret;
	}

	/**
	 * @param string $secret
	 * @return RadNas
	 */
	public function setSecret(string $secret): RadNas {
		$this->secret = $secret;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getServer(): string {
		return $this->server;
	}

	/**
	 * @param string $server
	 * @return RadNas
	 */
	public function setServer(string $server): RadNas {
		$this->server = $server;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getCommunity(): string {
		return $this->community;
	}

	/**
	 * @param string $community
	 * @return RadNas
	 */
	public function setCommunity(string $community): RadNas {
		$this->community = $community;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getDescription(): string {
		return $this->description;
	}

	/**
	 * @param string $description
	 * @return RadNas
	 */
	public function setDescription(string $description): RadNas {
		$this->description = $description;

		return $this;
	}
}
