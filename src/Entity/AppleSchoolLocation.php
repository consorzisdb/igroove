<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class AppleSchoolLocation {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", nullable=false)
	 */
	protected $name;

	/**
	 * @var AppleSchool
	 * @ORM\ManyToOne(targetEntity="AppleSchool", inversedBy="locations")
	 */
	protected $appleSchool;

	/**
	 * @var ArrayCollection|Provider[]
	 * @ORM\OneToMany(targetEntity="Provider", mappedBy="appleSchoolLocation")
	 **/
	private $providers;

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return mixed
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getFullName() {
		return $this->name.($this->getAppleSchool() instanceof AppleSchool ? " (".$this->getAppleSchool()->getOrganizationName().")":"");
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * @return AppleSchool
	 */
	public function getAppleSchool() {
		return $this->appleSchool;
	}

	/**
	 * @param AppleSchool $appleSchools
	 */
	public function setAppleSchool($appleSchool) {
		$this->appleSchool = $appleSchool;
	}

	/**
	 * @return ArrayCollection|Provider[]
	 */
	public function getProviders() {
		return $this->providers;
	}

	/**
	 * @param ArrayCollection|Provider[] $providers
	 */
	public function setProviders($providers) {
		$this->providers = $providers;
	}

}