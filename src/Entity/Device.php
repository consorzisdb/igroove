<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\DeviceRepository")
 * @UniqueEntity("mac")
 */
class Device
{
    /**
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $device;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\Regex("/^([a-fA-F0-9]{2}[:|\-]?){6}$/")
     */
    private $mac;

    /**
     * @ORM\ManyToOne(targetEntity="MikrotikList", inversedBy="macs")
     * @ORM\OrderBy({"nome" = "ASC"})
     **/
    private $mikrotikList;

    /**
     * @ORM\OneToMany(targetEntity="DeviceIp", mappedBy="mac", cascade={"persist"})
     * @Assert\All({
     *     @UniqueEntity(fields={"ip"},
     *     errorPath="ip",
     *     message="L'ip scelto è già in uso."),
     *     @Assert\Callback({"App\Entity\DeviceIp", "validate"})
     * })
     */
    private $ips;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $bypassHotspot;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $active;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ips = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set device
     *
     * @param string $device
     *
     * @return Device
     */
    public function setDevice($device)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device
     *
     * @return string
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * Set mac
     *
     * @param string $mac
     *
     * @return Device
     */
    public function setMac($mac)
    {
        $this->mac = $mac;

        return $this;
    }

    /**
     * Get mac
     *
     * @return string
     */
    public function getMac()
    {
        return $this->mac;
    }

    /**
     * Set ips
     *
     * @param string $ips
     *
     * @return Device
     */
    public function setIps($ips)
    {
        $this->ips = $ips;

        return $this;
    }

    /**
     * Get ips
     *
     * @return [DeviceIp]
     */
    public function getIps()
    {
        return $this->ips;
    }

    public function addIp(DeviceIp $deviceIp)
    {
        $deviceIp->setMac($this);
        $this->ips->add($deviceIp);
    }

    public function removeIp(DeviceIp $deviceIp) {
        $this->ips->removeElement($deviceIp);
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Device
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set mikrotikList
     *
     * @param \App\Entity\MikrotikList $mikrotikList
     *
     * @return Device
     */
    public function setMikrotikList(\App\Entity\MikrotikList $mikrotikList = null)
    {
        $this->mikrotikList = $mikrotikList;

        return $this;
    }

    /**
     * Get mikrotikList
     *
     * @return \App\Entity\MikrotikList
     */
    public function getMikrotikList()
    {
        return $this->mikrotikList;
    }

    /**
     * Set bypassHotspot
     *
     * @param boolean $bypassHotspot
     *
     * @return Device
     */
    public function setBypassHotspot($bypassHotspot)
    {
        $this->bypassHotspot = $bypassHotspot;

        return $this;
    }

    /**
     * Get bypassHotspot
     *
     * @return boolean
     */
    public function getBypassHotspot()
    {
        return $this->bypassHotspot;
    }
}
